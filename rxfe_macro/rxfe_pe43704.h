// ------------------------------------------------------------------
// ---- PE43704 ----
// The serial-addressable interface is a 16-bit serial-in,
// parallel-out shift register buffered by a transparent
// latch. The 16-bits make up two words comprised of 8-
// bits each. The first word is the Attenuation Word, which
// controls the state of the DSA. The second word is the
// Address Word, which is compared to the static (or
// programmed) logical states of the A0, A1 and A2 digital
// inputs. If there is an address match, the DSA changes
// state; otherwise its current state will remain unchanged.
// Address forced value: A0 = A1 = A2 = 0
// Example: Attenuation Word: Multiply by 4 and convert to binary → 4 * 18.25 dB → 73 → 01001001
// PE43704_ATT(x) ((uint8_t(ceil(x*4)) & 0x7F))
#define PE43704_ATT(x) ((((x*4)) & 0x7F))
