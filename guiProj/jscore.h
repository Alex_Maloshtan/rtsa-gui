#ifndef JSCORE_H
#define JSCORE_H

#include <QObject>
#include <QDebug>
#include <QFile>
class JSCore : public QObject
{
    Q_OBJECT

    //Q_PROPERTY(int aaa READ getA WRITE setA NOTIFY aChanged SCRIPTABLE true)
public:
    explicit JSCore(QObject *parent = 0);
    Q_INVOKABLE
    uint32_t get32Word(const QString binaryString);

signals:
    void aChanged(int);
public slots:
private:

private slots:

};

#endif // JSCORE_H
