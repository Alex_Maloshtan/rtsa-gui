#ifndef BINARYFILEGENERATOR_H
#define BINARYFILEGENERATOR_H
#include <QVector>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <defines.h>
#include <QDebug>

//#define CU_BASE_ADDR 0xA0000000
#define CU_BASE_ADDR 0x88000000

class BinaryFIleGenerator
{
private:
    uint32_t addr = 0x0000;
    int currentRow = 0;
    QVector<uint32_t> rowsAddrs;
    QVector<int> jumpTo;
    QByteArray binFile;

    QFile * fid_addr0;
    QByteArray addr0;

    QFile * modeSettings;
    QFile * bin;

    QFile * startAddresses;
    QTextStream * startAddressesText;

    QVector<uint32_t> delays;
    QVector<uint32_t> triggers;
    QVector<uint32_t> muxSettings;
    QVector<uint32_t> ads1Bytes;
    QVector<uint32_t> ads2Bytes;

    QString ICname;

    uint32_t settingsOffset ;
    QByteArray settingsBinary;
    bool nextICflag = true;
    void generateSettings();
    QByteArray generateBinarySettings();

    // Controller IP definitions
    // TODO: Should be refer to xparameters.h

    const unsigned int cu_addr[10] = {
                                        CU_BASE_ADDR + 0x1000,  // ADFR6520     CTL1
                                        CU_BASE_ADDR + 0x0000,  // ADS5562      CTL0
                                        CU_BASE_ADDR + 0x3000,  // HMC625       CTL3

                                        CU_BASE_ADDR + 0x6000,  // LXM2592_1    CTL6
                                        CU_BASE_ADDR + 0x2000,  // LXM2592_0    CTL2
                                        CU_BASE_ADDR + 0x4000,  // LTC2624      CTL4

                                        CU_BASE_ADDR + 0x5000,  // LTC5586      CTL5
                                        CU_BASE_ADDR + 0x7000,  // PE43704      CTL7
                                        CU_BASE_ADDR + 0x8000,  // DIGITAL      CTL8

                                        CU_BASE_ADDR + 0x9000,  // BRAM memory addr
                                        //CU_BASE_ADDR + 0xA000,  // Modes setting address // Not supported yet
                                        //CU_BASE_ADDR + 0x0000   // ?
                                     };
public:
    uint32_t ICnum;
    void addMode(QString nextMode,QVector<uint32_t> ctrlWords);
    void addSetting(uint32_t delay,uint32_t mux,uint32_t trigger,uint32_t ads1bytes,uint32_t ads2bytes);
    void nextIC(QString ICname);
    BinaryFIleGenerator(QString fileName);
    ~BinaryFIleGenerator();
};

#endif // BINARYFILEGENERATOR_H
