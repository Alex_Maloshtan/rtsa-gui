#include "binaryfilegenerator.h"

void BinaryFIleGenerator::generateSettings()
{
    uint32_t currentSettingsAddress = 0;
       QTextStream modeSettingsText(modeSettings);
       modeSettingsText << "mux settings: 0=no mux 1=mux1 2=mux2 3=mux1+2\n";
       for (int i=0;i<delays.size();i++){
            modeSettingsText << "\nmode: " << i << "\n\tdelay: "<< delays[i];
            modeSettingsText << "\n\ttrigger: " << triggers[i];
            modeSettingsText << "\n\tmux: "<<QString::number(muxSettings[i]);
            modeSettingsText << "\nadr[0x"+QString::number(settingsOffset+currentSettingsAddress,16) + "] = 0x"
                             << QString::number(*((uint32_t*)(settingsBinary.constData()+currentSettingsAddress)),16) << "\n";
            currentSettingsAddress+=4;
            modeSettingsText << "\n\tADS54J60 bytes: ";
            modeSettingsText << QString::number(ads1Bytes[i]);
            modeSettingsText << "\nadr[0x"+QString::number(settingsOffset+currentSettingsAddress,16) + "] = 0x"
                             << QString::number(*((uint32_t*)(settingsBinary.constData()+currentSettingsAddress)),16) << "\n";
            currentSettingsAddress+=4;
            modeSettingsText << "\n\tADS5562 bytes: ";
            modeSettingsText << QString::number(ads2Bytes[i]);
            modeSettingsText << "\nadr[0x"+QString::number(settingsOffset+currentSettingsAddress,16) + "] = 0x"
                             << QString::number(*((uint32_t*)(settingsBinary.constData()+currentSettingsAddress)),16) << "\n";
            currentSettingsAddress+=4;
            modeSettingsText << "\nadr[0x"+QString::number(settingsOffset+currentSettingsAddress,16) + "] = 0x"
                             << QString::number(*((uint32_t*)(settingsBinary.constData()+currentSettingsAddress)),16) << "(next adr)\n";
            currentSettingsAddress+=4;

       }

}

QByteArray BinaryFIleGenerator::generateBinarySettings()
{
    QTextStream modeSettingsText(this->modeSettings);
    QVector<uint32_t> rowAddrs;
    QByteArray modeSettings;
    rowAddrs.reserve(muxSettings.size());
    settingsOffset = addr;
    qDebug() << QString::number(addr);
    for (int i=0;i<muxSettings.size();i++){
        uint32_t muxAndDelay = muxSettings[i];
        muxAndDelay = muxAndDelay << 30;
        if (triggers[i]==TRIGGER_EN){
            muxAndDelay |= 1 << 29;
        }
        muxAndDelay |= (0x1FFFFFFF & delays[i]);
        modeSettings.append((char*)&muxAndDelay,sizeof(uint32_t));
        uint32_t ads1 = ads1Bytes[i];
        uint32_t ads2 = ads2Bytes[i];
        modeSettings.append((char*)&ads1,sizeof(uint32_t));
        modeSettings.append((char*)&ads2,sizeof(uint32_t));
        uint32_t nextSettingsAddress = settingsOffset + jumpTo[i]*16;
        modeSettings.append((char*)&nextSettingsAddress,sizeof(uint32_t));
    }
    return modeSettings;
}

void BinaryFIleGenerator::addMode(QString nextMode, QVector<uint32_t> ctrlWords)
{
    QTextStream modeSettingsText(modeSettings);
    if (nextICflag){
        *startAddressesText <<"0x"<< QString::number(addr,16)<< " " << ICname <<"\n";
        modeSettingsText << ICname+":\n";
        nextICflag = false;

        // Write initial adresses into binary file as {cu_addr, addr} pairs
        addr0.append((char*)&cu_addr[ICnum],sizeof(uint32_t));
        addr0.append((char*)&addr,sizeof(uint32_t));
    }

    rowsAddrs.push_back(addr);
    jumpTo.push_back(0);
    modeSettingsText << "    Mode " << QString::number(currentRow)<< "\n";
    // Arrange control words in memory
    if (!ctrlWords.isEmpty()) {
        foreach (int32_t words, ctrlWords) {
            modeSettingsText << QString::number(addr, 16).toUtf8().constData()
                     << "        cmd[0x" << QString::number(addr, 16).toUtf8().constData()
                     << "] = 0x" << QString::number(words, 16).toUtf8().constData()<< "\n";
            binFile.append((char*)&words,sizeof(uint32_t));
            addr += 4;
        }
    }
    // Next addr selection
    if (nextMode == QString("stop")) {
        // set stop
        modeSettingsText << "        NEXT: STOP" << "\n";
        modeSettingsText << QString::number(addr, 16).toUtf8().constData()
                 << "        next[0x" << QString::number(addr, 16).toUtf8().constData()
                 << "] = 0x" << QString::number(addr , 16).toUtf8().constData() << "\n";
        uint32_t jmpOpCode = addr | 0x80000000;
        binFile.append((char*)&jmpOpCode,sizeof(uint32_t));
        addr += 4;
        jumpTo[currentRow] = currentRow;
    } else {
        bool isInt = false;
        int nextMode_ = nextMode.toInt(&isInt) - 1;
        if (isInt) {
            // Number entered
        } else {
            // Void field
            nextMode_ = currentRow + 1;
        }
        qDebug() << "           NEXT: " << QString::number(nextMode_).toUtf8().constData();

        // Addr selection
        if (nextMode_ > currentRow) {
            // Simply add addr
            modeSettingsText << QString::number(addr, 16).toUtf8().constData()
                     << "        next[0x" << QString::number(addr, 16).toUtf8().constData()
                     << "] = 0x" << QString::number(addr + 4, 16).toUtf8().constData()<< "\n";
            uint32_t jmpOpCode = (addr+4) | 0x80000000;
            binFile.append((char*)&jmpOpCode,sizeof(uint32_t));
            addr += 4;
            jumpTo[currentRow] = currentRow+1;
            // Store addr to table
            //rowsAddrs[row+1] = addr;
        } else {
            // Look in addr table
            modeSettingsText << QString::number(addr, 16).toUtf8().constData()
                     << "        next[0x" << QString::number(addr, 16).toUtf8().constData()
                     << "] = 0x" << QString::number(rowsAddrs[nextMode_], 16).toUtf8().constData()<< "\n";
            uint32_t jmpOpCode = rowsAddrs[nextMode_] | 0x80000000;
            binFile.append((char*)&jmpOpCode,sizeof(uint32_t));
            addr += 4;
            jumpTo[currentRow] = nextMode_;
        }
    }
    currentRow++;
}

void BinaryFIleGenerator::addSetting(uint32_t delay, uint32_t mux, uint32_t trigger, uint32_t ads1bytes, uint32_t ads2bytes)
{
    delays.push_back(delay);
    muxSettings.push_back(mux);
    triggers.push_back(trigger);
    ads1Bytes.push_back(ads1bytes);
    ads2Bytes.push_back(ads2bytes);
}

void BinaryFIleGenerator::nextIC(QString ICname)
{
    this->ICname = ICname;
    nextICflag = true;
    currentRow = 0;
    rowsAddrs.clear();
}

BinaryFIleGenerator::BinaryFIleGenerator(QString fileName)
{
    modeSettings = new QFile(fileName + ".log");
    modeSettings->open(QIODevice::WriteOnly | QIODevice::Truncate);

    bin = new QFile(fileName + ".bin");
    bin->open(QIODevice::WriteOnly | QIODevice::Truncate);

    startAddresses = new QFile("startAddresses");
    startAddresses->open(QIODevice::WriteOnly | QIODevice::Truncate);
    startAddressesText = new QTextStream(startAddresses);

    // Open file for initial addresses pairs storage
    addr0.clear();
    fid_addr0 = new QFile(fileName + "addr0.bin");
    fid_addr0->open(QIODevice::WriteOnly | QIODevice::Truncate);
}

BinaryFIleGenerator::~BinaryFIleGenerator()
{
    // Generate modes binary image
    settingsBinary = generateBinarySettings();
    generateSettings();

    // Insert header into binary file: addr and len
    uint32_t bin_len = binFile.size()/4 + settingsBinary.size()/4;
    binFile.prepend((char*)&bin_len,sizeof(uint32_t));

    uint32_t bin_addr = cu_addr[9];
    binFile.prepend((char*)&bin_addr,sizeof(uint32_t));

    // Write modules data
    bin->write(binFile.constData(),binFile.size());
    // Write modes data
    bin->write(settingsBinary.constData(),settingsBinary.size());
    bin->close();

    modeSettings->close();
    startAddresses->close();

    // Add modes setting address    
//    uint32_t modes_addr = cu_addr[10];
//    uint32_t modes_addr0 = binFile.size();
//    addr0.append((char*)&modes_addr,sizeof(uint32_t));
//    addr0.append((char*)&modes_addr0,sizeof(uint32_t));

    // Add number of pair into addr0
    uint32_t pairs_num = addr0.size()/(2*sizeof(uint32_t));
    addr0.prepend((char*)&pairs_num,sizeof(uint32_t));

    fid_addr0->write(addr0.constData(),addr0.size());
    fid_addr0->close();

    delete bin;
    delete modeSettings;
    delete startAddresses;
    delete startAddressesText;
    delete fid_addr0;

}
