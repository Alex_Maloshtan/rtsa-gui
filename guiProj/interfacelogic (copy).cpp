#include "interfacelogic.h"

interfaceLogic::interfaceLogic()
{
    eng.installExtensions(QJSEngine::ConsoleExtension);
    QJSValue val = eng.newQObject(&core);
    eng.globalObject().setProperty("core",val);
    for (int i=0;i<9;i++){
        QVector<uint32_t> a;
        ctrlWords.push_back(a);
    }

    for (int i=0;i<8;i++){
        readFromFile(fileNames[i]);
    }
//    readFromFile("rxfe_ltc5586.h");
}

void interfaceLogic::execScript(QString text, const int chip)
{
    uint32_t word = -1;
    QStringList evaluateCmds = text.split("\n");
    if (text.isEmpty()){
        return;
    }
    if (chip== LXM2592_1 || chip== LXM2592_2 || chip== LTC5586 || chip== ADS5562 || chip == PE43704 || chip== LTC2624){
    foreach (QString evalText, evaluateCmds) {
        if (evalText.isEmpty()){
            continue;
        }
        QJSValue js_value = eng.evaluate(evalText);
                if(js_value.isError()){
                    qCritical() << QString("name %1:%2: %3")
                                   .arg(chip)
                                   .arg(js_value.property("lineNumber").toString())
                                   .arg(js_value.property("message").toString()).toUtf8().constData();
                } 
        if (js_value.isNumber()) {
            word = js_value.toNumber();
        }
        ctrlWords[chip].push_back(word);
        qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalText)
                                            .arg(QString::number(word,16))
                                            .arg(QString::number(word,2))
                                            .arg(chipNames[chip]);
    }
    }
    if (chip== ADFR6520){

        QString evalOr;
        foreach (QString evalText, evaluateCmds) {
            evalOr.append(evalText);
            evalOr.append(" |");
        }
        evalOr.chop(1);
        QJSValue js_value = eng.evaluate(evalOr);
                if(js_value.isError()){
                    qCritical() << QString("name %1:%2: %3")
                                   .arg(chip)
                                   .arg(js_value.property("lineNumber").toString())
                                   .arg(js_value.property("message").toString()).toUtf8().constData();
                }
        if (js_value.isNumber()) {
             word = js_value.toNumber();
        }
        ctrlWords[chip].push_back(word);
        qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalOr)
                                            .arg(QString::number(word,16))
                                            .arg(QString::number(word,2))
                                            .arg(chipNames[chip]);
    }
    if (chip== HMC625){
        QString evalAnd;
        foreach (QString evalText, evaluateCmds) {
            evalAnd.append(evalText);
            evalAnd.append(" &");
        }
        evalAnd.chop(1);
        QJSValue js_value = eng.evaluate(evalAnd);
                if(js_value.isError()){
                    qCritical() << QString("name %1:%2: %3")
                                   .arg(chip)
                                   .arg(js_value.property("lineNumber").toString())
                                   .arg(js_value.property("message").toString()).toUtf8().constData();
                }
        if (js_value.isNumber()) {
             word = js_value.toNumber();
        }
        ctrlWords[chip].push_back(word);
        qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalAnd)
                                            .arg(QString::number(word,16))
                                            .arg(QString::number(word,2))
                                            .arg(chipNames[chip]);
    }
    if(chip== DIGITAL){
        eng.evaluate("var inputWord = 0");
        foreach (QString evalText, evaluateCmds) {
            QJSValue js_value = eng.evaluate("inputWord = "+ evalText +"(inputWord)");
                    if(js_value.isError()){
                        qCritical() << QString("name %1:%2: %3")
                                       .arg(chip)
                                       .arg(js_value.property("lineNumber").toString())
                                       .arg(js_value.property("message").toString()).toUtf8().constData();
                    }
                    js_value = eng.evaluate("inputWord");
                    if (js_value.isNumber()) {
                        word = js_value.toNumber();
                    }
                    qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalText)
                                                        .arg(QString::number(word,16))
                                                        .arg(QString::number(word,2))
                                                        .arg(chipNames[chip]);
        }
        ctrlWords[chip].push_back(word);
    }

}

void interfaceLogic::addScenario(QVector<ntable *> & ntables, QTableWidget * scenarioList)
{
    ntable * table= new ntable();
    ntables.push_back(table);
    const int lastRow = scenarioList->rowCount();
    scenarioList->insertRow(lastRow);
    table->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    for (int i=0;i<scenarioList->columnCount();i++){
        scenarioList->setItem(lastRow,i,new QTableWidgetItem());
    }
    scenarioList->item(lastRow,0)->setText("mode "+QString::number(scenarioList->rowCount()));
    jumpTo.push_back(scenarioList->item(lastRow,2)->text().toInt()-1);
}

void interfaceLogic::delScenario(QVector<ntable *> &ntables, QTableWidget *scenarioList, QGridLayout *gridLayout)
{
    if(scenarioList->rowCount() == 1) return;
    if (scenarioList->selectedItems().isEmpty()) return;
    const int row = scenarioList->row(scenarioList->selectedItems().at(0));
    QLayoutItem * child= gridLayout->takeAt(6);
    if (child){
    child->widget()->hide();
    gridLayout->removeWidget(child->widget());
    gridLayout->removeItem(child);
    delete child;
    }
    scenarioList->removeRow(row);
    delete ntables[row];
    ntables.remove(row);
}

int interfaceLogic::moveUp(QVector<ntable *> &ntables, QTableWidget *scenarioList)
{
    const int sourceRow = scenarioList->row(scenarioList->selectedItems().at(0));
    const int destRow = sourceRow-1;
    if (destRow < 0) return 0;
    for (int i=0;i<scenarioList->columnCount();i++){
        QTableWidgetItem * src = scenarioList->item(sourceRow,i)->clone();
        QTableWidgetItem * dst = scenarioList->item(destRow,i)->clone();
        //delete table->item(sourceRow,i)
        scenarioList->setItem(sourceRow,i,dst);
        scenarioList->setItem(destRow,i,src);
    }
    ntable * temp = ntables.at(sourceRow);
    ntables.replace(sourceRow,ntables.at(destRow));
    ntables.replace(destRow,temp);
    scenarioList->selectRow(destRow);
    return destRow;
}

int interfaceLogic::moveDown(QVector<ntable *> &ntables, QTableWidget *scenarioList)
{
    const int sourceRow = scenarioList->row(scenarioList->selectedItems().at(0));
    const int destRow = sourceRow+1;
    if (destRow >= scenarioList->rowCount()) return scenarioList->rowCount() - 1;
    for (int i=0;i<scenarioList->columnCount();i++){
        QTableWidgetItem * src = scenarioList->item(sourceRow,i)->clone();
        QTableWidgetItem * dst = scenarioList->item(destRow,i)->clone();
        delete scenarioList->item(sourceRow,i);
        delete scenarioList->item(destRow,i);
        scenarioList->setItem(sourceRow,i,dst);
        scenarioList->setItem(destRow,i,src);
    }
    ntable * temp = ntables.at(sourceRow);
    ntables.replace(sourceRow,ntables.at(destRow));
    ntables.replace(destRow,temp);
    scenarioList->selectRow(destRow);
    return destRow;
}

void interfaceLogic::writeSettings(QVector<ntable *> &ntables, QTableWidget *table)
{
    QCoreApplication::setOrganizationName("123");
    QCoreApplication::setApplicationName("guiProj");
    QString debug = settings.fileName();
    settings.setValue("tabsNum",table->rowCount()-1);//1 уже создаем по-умолчанию
    settings.setValue("tabsColumns",table->columnCount());
    for(int i=0;i<table->rowCount();i++){
        for(int j=0;j<table->columnCount();j++){
            settings.setValue("cell"+QString::number(i)+"_"+QString::number(j),table->item(i,j)->text());
        }
        ntables[i]->writeSettings(i);
    }
    if (table->selectedItems().isEmpty()){
        settings.setValue("currentTabRow",0);
    } else {
        settings.setValue("currentTabRow",table->row(table->selectedItems().at(0)));
    }
}

void interfaceLogic::readSettings(QVector<ntable *> &ntables, QTableWidget *table)
{
    int tabsNum,tabsColumns;
    tabsNum = settings.value("tabsNum").toInt();
    tabsColumns = settings.value("tabsColumns").toInt();
    jumpTo.reserve(tabsNum);
    for (int i=0;i<tabsNum;i++){
        addScenario(ntables,table);
    }
    for(int i=0;i<table->rowCount();i++){
        for(int j=0;j<table->columnCount();j++){
            QString cellText = settings.value("cell"+QString::number(i)+"_"+QString::number(j)).toString();
            table->item(i,j)->setText(cellText);
        }
        ntables[i]->readSettings(i);
    }
    table->selectRow(settings.value("currentTabRow").toInt());

}

int interfaceLogic::listClicked(int row ,QVector<ntable *> & ntables,QGridLayout * gridLayout)
{
    QLayoutItem * child= gridLayout->takeAt(6);
    if (child){
        child->widget()->hide();
        gridLayout->removeWidget(child->widget());
        gridLayout->removeItem(child);
        delete child;
    }
    gridLayout->addWidget(ntables[row],0,5,8,10);
    gridLayout->setColumnStretch(5,2);
    ntables[row]->show();
    return row;
}

void interfaceLogic::setupScenarioList(QTableWidget *table)
{
    table->setColumnCount(3);
    table->setHorizontalHeaderItem(2,new QTableWidgetItem("Branch"));
    table->verticalHeader()->hide();
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void interfaceLogic::readFromFile(QString fileName)
{
    headerFile.setFileName(fileName);
    if(!headerFile.open(QIODevice::ReadOnly|QIODevice::Text)){
        qWarning() << QString("header file %1 not found!").arg(fileName);
        return;
    }
    QRegExp rx("\\s*#define ");
    QRegExp function("^\\S+[(]\\S+[)]");
    while(!headerFile.atEnd()){
    QString line = headerFile.readLine() ;
    int defPos = rx.indexIn(line);
        if (defPos != -1){
            line.remove(0,8);   //remove define
            if (function.indexIn(line) != -1){
                const int functionLastIndex = function.indexIn(line)+function.matchedLength();
                line.insert(functionLastIndex,"{return ");
                line.chop(1);
                line.append("; }");
                line.insert(0,"function ");
                //is function
            } else {
                int spaceIndex = line.indexOf(' ');
                line.replace(spaceIndex,1,QChar('='));
                line.chop(1);
                line.append(";");
                line.insert(0,"var ");
            }            
//            qDebug() << QString("%1:%2: %3: %4")
//                        .arg(__FILE__)
//                        .arg(__LINE__)
//                        .arg(__FUNCTION__)
//                        .arg(line).toUtf8().constData();

            QJSValue val = eng.evaluate(line);
            if(val.isError()){
                qCritical() << line;
                qCritical() << QString("%1:%2: %3")
                               .arg(headerFile.fileName())
                               .arg(val.property("lineNumber").toString())
                               .arg(val.property("message").toString()).toUtf8().constData();
            }
        }

    }
    headerFile.close();
}

void interfaceLogic::execAllScripts(QVector<ntable *> ntables,QTableWidget * scenarioTable)
{
    QVector<QString> modeNames;
    for(int row=0;row< scenarioTable->rowCount();row++){
    jumpTo[row] = scenarioTable->item(row,2)->text().toInt()-1;
        for (int i=0;i<9;i++){
            execScript(ntables[row]->inputs[i]->document()->toPlainText(),i);
        }
        ctrlWordsForMode.push_back(ctrlWords);
        ctrlWords.clear();
        for (int i=0;i<9;i++){ //reserve не работает
            QVector<uint32_t> a;
        ctrlWords.push_back(a);
        }
        modeNames.push_back(scenarioTable->item(row,0)->text());
    }
    binaryFile file(ctrlWordsForMode,modeNames,jumpTo);
    file.saveToTextFile();
    ctrlWordsForMode.clear();
}

void interfaceLogic::onScenarioCellChanged(int row, int column, QString value)
{
//    int branchval = value.toInt() - 1;//0 - первый режим
//    jumpTo[row] = branchval;
}






