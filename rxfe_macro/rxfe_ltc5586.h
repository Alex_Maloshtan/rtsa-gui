// ------------------------------------------------------------------
// ---- LTC5586 -----
#define W(addr,data)    ((0<<15) | (((addr) & 0x7F) << 8) | ((data) & 0xFF))
#define R(addr)    		((1<<15) | (((addr) & 0x7F) << 8))

// IM3QX[7:0] IM3 Q-Channel X-Vector Controls the Q-channel IM3 X-vector adjustment. 0x00 to 0xFF 0x80
// 0x00 IM3QY[7] IM3QY[6] IM3QY[5] IM3QY[4] IM3QY[3] IM3QY[2] IM3QY[1] IM3QY[0] R/W 0x80
#define IM3QY(x) W(0x00, x)

// 0x01 IM3QX[7] IM3QX[6] IM3QX[5] IM3QX[4] IM3QX[3] IM3QX[2] IM3QX[1] IM3QX[0] R/W 0x80
// IM3QX[7:0] IM3 Q-Channel X-Vector Controls the Q-channel IM3 X-vector adjustment. 0x00 to 0xFF 0x80
#define IM3QX(x) W(0x01, x)

// 0x02 IM3IY[7] IM3IY[6] IM3IY[5] IM3IY[4] IM3IY[3] IM3IY[2] IM3IY[1] IM3IY[0] R/W 0x80
// IM3IY[7:0] IM3 I-Channel Y-Vector Controls the I-channel IM3 Y-vector adjustment. 0x00 to 0xFF 0x80
#define IM3IY(x) W(0x02, x)

// 0x03 IM3IX[7] IM3IX[6] IM3IX[5] IM3IX[4] IM3IX[3] IM3IX[2] IM3IX[1] IM3IX[0] R/W 0x80
// IM3IX[7:0] IM3 I-Channel X-Vector Controls the I-channel IM3 X-vector adjustment. 0x00 to 0xFF 0x80
#define IM3IX(x) W(0x03, x)

// 0x04 IM2QX[7] IM2QX[6] IM2QX[5] IM2QX[4] IM2QX[3] IM2QX[2] IM2QX[1] IM2QX[0] R/W 0x80
// IM2QX[7:0] IM2 Q-Channel X-Vector Controls the Q-channel IM2 X-vector adjustment. 0x00 to 0xFF 0x80
#define IM2QX(x) W(0x04, x)

// 0x05 IM2IX[7] IM2IX[6] IM2IX[5] IM2IX[4] IM2IX[3] IM2IX[2] IM2IX[1] IM2IX[0] R/W 0x80
// IM2IX[7:0] IM2 I-Channel X-Vector Controls the I-channel IM2 X-vector adjustment. 0x00 to 0xFF 0x80
#define IM2IX(x) W(0x05, x)

// 0x06 HD3QY[7] HD3QY[6] HD3QY[5] HD3QY[4] HD3QY[3] HD3QY[2] HD3QY[1] HD3QY[0] R/W 0x80
// HD3QY[7:0] HD3 Q-Channel Y-Vector Controls the Q-channel HD3 Y-vector adjustment. 0x00 to 0xFF 0x80
#define HD3QY(x) W(0x06, x)

// 0x07 HD3QX[7] HD3QX[6] HD3QX[5] HD3QX[4] HD3QX[3] HD3QX[2] HD3QX[1] HD3QX[0] R/W 0x80
// HD3QX[7:0] HD3 Q-Channel X-Vector Controls the Q-channel HD3 X-vector adjustment. 0x00 to 0xFF 0x80
#define HD3QX(x) W(0x07, x)

// 0x08 HD3IY[7] HD3IY[6] HD3IY[5] HD3IY[4] HD3IY[3] HD3IY[2] HD3IY[1] HD3IY[0] R/W 0x80
// HD3IY[7:0] HD3 I-Channel Y-Vector Controls the I-channel HD3 Y-vector adjustment. 0x00 to 0xFF 0x80
#define HD3IY(x) W(0x08, x)

// 0x09 HD3IX[7] HD3IX[6] HD3IX[5] HD3IX[4] HD3IX[3] HD3IX[2] HD3IX[1] HD3IX[0] R/W 0x80
// HD3IX[7:0] HD3 I-Channel X-Vector Controls the I-channel HD3 X-vector adjustment. 0x00 to 0xFF 0x80
#define HD3IX(x) W(0x09, x)

// 0x0A HD2QY[7] HD2QY[6] HD2QY[5] HD2QY[4] HD2QY[3] HD2QY[2] HD2QY[1] HD2QY[0] R/W 0x80
// HD2QY[7:0] HD2 Q-Channel Y-Vector Controls the Q-channel HD2 Y-vector adjustment. 0x00 to 0xFF 0x80
#define HD2QY(x) W(0x0A, x)

// 0x0B HD2QX[7] HD2QX[6] HD2QX[5] HD2QX[4] HD2QX[3] HD2QX[2] HD2QX[1] HD2QX[0] R/W 0x80
// HD2QX[7:0] HD2 Q-Channel X-Vector Controls the Q-channel HD2 X-vector adjustment. 0x00 to 0xFF 0x80
#define HD2QX(x) W(0x0B, x)

// 0x0C HD2IY[7] HD2IY[6] HD2IY[5] HD2IY[4] HD2IY[3] HD2IY[2] HD2IY[1] HD2IY[0] R/W 0x80
// HD2IY[7:0] HD2 I-Channel Y-Vector Controls the I-channel HD2 Y-vector adjustment. 0x00 to 0xFF 0x80
#define HD2IY(x) W(0x0C, x)

// 0x0D HD2IX[7] HD2IX[6] HD2IX[5] HD2IX[4] HD2IX[3] HD2IX[2] HD2IX[1] HD2IX[0] R/W 0x80
// HD2IX[7:0] HD2 I-Channel X-Vector Controls the I-channel HD2 X-vector adjustment. 0x00 to 0xFF 0x80
#define HD2IX(x) W(0x0D, x)

// 0x0E DCOI[7]  DCOI[6] DCOI[5] DCOI[4] DCOI[3] DCOI[2] DCOI[1] DCOI[0] R/W 0x80
// DCOI[7:0] I-Channel DC Offset Controls the I-channel DC offset over a range from –200mV to 200mV. 0x00 to 0xFF 0x80
#define DCOI(x) W(0x0E, x)

// 0x0F DCOQ[7]  DCOQ[6] DCOQ[5] DCOQ[4] DCOQ[3] DCOQ[2] DCOQ[1] DCOQ[0] R/W 0x80
// DCOQ[7:0] Q-Channel DC Offset Controls the Q-channel DC offset over a range from –200mV to 200mV. 0x00 to 0xFF 0x80
#define DCOQ(x) W(0x0F, x)

// 0x10 ATT[4] ATT[3] ATT[2] ATT[1] ATT[0] IP3IC[2] IP3IC[1] IP3IC[0] R/W 0x04
// TT[4:0] Step Attenuator Control Controls the step attenuator from 0dB to 31dB attenuation. 0x00 to 0x1F 0x00
#define R10_ATT(x) W(0x10, (x & 0x1F) << 3)
// IP3IC[2:0] RF Input IP3 IC Adjust Used to optimize the RF input IP3. 0x00 to 0x07 0x04
#define R10_IP3_IC(x) W(0x10, (x & 0x07) )

// 0x11 GERR[5] GERR[4] GERR[3] GERR[2] GERR[1] GERR[0] IP3CC[1] IP3CC[0] R/W 0x82
// GERR[5:0] IQ Gain Error Adjust Controls the IQ gain error over a range from –0.5dB to 0.5dB. 0x00 to 0x3F 0x20
#define R11_GERR(x) W(0x11, (x & 0x3F) << 2)
// IP3CC[1:0] RF Input IP3 CC Adjust Used to optimize the RF input IP3. 0x00 to 0x03 0x02
#define R11_IP3_CC(x) W(0x11, (x & 0x03) )

// 0x12 LVCM[2] LVCM[1] LVCM[0] CF1[4] CF1[3] CF1[2] CF1[1] CF1[0] R/W 0x48
// LVCM[2:0] LO Bias Adjust Used to optimize mixer IP3. 0x00 to 0x07 0x02
#define R12_LVCM(x) W(0x12, (x & 0x07) << 5)
// CF1[5:0] LO Matching Capacitor CF1 Controls the CF1 capacitor in the LO matching network. 0x00 to 0x1F 0x08
#define R12_CF1(x) W(0x12, (x & 0x1F) )

// 0x13 BAND LF1[1] LF1[0] CF2[4] CF2[3] CF2[2] CF2[1] CF2[0] R/W 0xE3
// BAND LO Band Select Selects which LO matching band is used. BAND = 1 for high band. BAND = 0 for low band. 1
#define R13_HBAND W(0x13, 1 << 7)
#define R13_LBAND W(0x13, 0 << 7)
// LF1[1:0] LO Matching Inductor LF1 Controls the LF1 inductor in the LO matching network. 0x00 to 0x03 0x03
#define R13_LF1(x) W(0x13, (x & 0x03) << 5)
// CF2[5:0] LO Matching Capacitor CF2 Controls the CF2 capacitor in the LO matching network. 0x00 to 0x1F 0x03
#define R13_CF2(x) W(0x13, (x & 0x1F) )


// 0x14 PHA[8] PHA[7] PHA[6] PHA[5] PHA[4] PHA[3] PHA[2] PHA[1] R/W 0x80
// PHA[8:0] IQ Phase Error Adjust Controls the IQ phase error over a range from –2.5 Degrees to 2.5 Degrees. 0x000 to 0x1FF 0x100
#define PHA_MSB(x) W(0x14, (x << 1))

// 0x15 PHA[0] AMPG[2] AMPG[1] AMPG[0] AMPCC[1] AMPCC[0] AMPIC[1] AMPIC[0] R/W 0x6A
#define R15_PHA_LSB(x) W(0x15, (x & 1) << 7)
// AMPG[2:0] IF Amplifier Gain Adjust Adjusts the amplifier gain from 8dB to 15dB. 0x00 to 0x07 0x06
#define R15_AMPG(x) W(0x15, (x & 0x07) << 4)
// AMPCC[3:2]
#define R15_AMPCC(x) W(0x15, (x & 0x03) << 2)
// AMPIC[1:0]
#define R15_AMPIC(x) W(0x15, (x & 0x03))

// 0x16 1* 1* 1* 1* SRST SDO_MODE 0* 0* R/W 0xF0
// SRST Soft Reset Writing 1 to this bit resets all registers to their default values. 0, 1 0
#define SW_RESET W(0x16, (0xF8))
// SDO_MODE SDO Readback Mode Enables the SDO readback mode if SDO_MODE = 1. 0, 1 0
#define READBACK_EN W(0x16, (0xF4))
#define READBACK_DISABLE W(0x16, (0xF4))

// 0x17 CHIPID[1] CHIPID[0] 0* 0* 0* 0* 0* RFSW R/W 0x01
// CHIPID Chip Identification Bits Factory set to default value. 0x00 to 0x03 0x00
#define CHIPID(x) W(0x17, ((x & 0x03) << 6))

#define RFSW0 W(0x17, (0))
#define RFSW1 W(0x17, (1))
