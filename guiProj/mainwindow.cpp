#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    new QShortcut(QKeySequence(Qt::Key_Insert),this,SLOT(onAddButton()));
    new QShortcut(QKeySequence(Qt::Key_Delete),this,SLOT(onDelButton()));//становится дочерним, нет утечки
    new QShortcut(QKeySequence(Qt::Key_Up+Qt::CTRL),this,SLOT(onMoveUp()));
    new QShortcut(QKeySequence(Qt::Key_Down+Qt::CTRL),this,SLOT(onMoveDown()));
    windowsStyle = QStyleFactory::create("windows");
    addButton = new QPushButton();
    addButton->setIcon(QIcon("add.png"));
    addButton->setStyle(windowsStyle);
    delButton = new QPushButton();
    delButton->setIcon(QIcon("remove.png"));
    delButton->setStyle(windowsStyle);
    moveUp = new QPushButton();
    moveUp->setIcon(QIcon("up.png"));
    moveUp->setStyle(windowsStyle);
    moveDown = new QPushButton();
    moveDown->setIcon(QIcon("down.png"));
    moveDown->setStyle(windowsStyle);
    execButton = new QPushButton("GENERATE");
    execButton->setStyle(windowsStyle);
    table = new QTableWidget();
    logic.setupScenarioList(table);
    logic.setLayoutPtr(ui->gridLayout);
    ui->gridLayout->setContentsMargins(0,0,0,0);
    ui->gridLayout->setSpacing(0);
    ui->gridLayout->addWidget(table,0,0,2,5);
    ui->gridLayout->addWidget(addButton,2,0,1,1);
    ui->gridLayout->addWidget(delButton,2,1,1,1);
    ui->gridLayout->addWidget(moveUp,2,2,1,1);
    ui->gridLayout->addWidget(moveDown,2,3,1,1);
    ui->gridLayout->addWidget(execButton,2,4,1,1);
    for(int i=0;i<5;i++){
        ui->gridLayout->setColumnStretch(i,1);
    }
    for(int i=0;i<9;i++){
        ui->gridLayout->setRowStretch(i,2);
    }
    menu = new QMenu(tr("&Scenarios"),this);
    ui->menuBar->addMenu(menu);
    console = new QTextEdit();
    console->setReadOnly(true);
    ui->gridLayout->addWidget(console,8,0,1,5);
    menu->addAction(tr("&Open scenario"),this,SLOT(readSettings()),tr("Ctrl+O"));
    menu->addAction(tr("&Save scenario"),this,SLOT(writeSettings()),tr("Ctrl+S"));
    menu->addSeparator();
    menu->addAction(tr("&Clear mode"),this,SLOT(onClearTable()),tr("Ctrl+Del"));
    menu->addAction(tr("&Clear scenario"),this,SLOT(onClearAll()),tr("Ctrl+Shift+Del"));
    menu->addAction(tr("&Exit"),this,SLOT(close()));
    menu->addSeparator();
    menu->show();
    //logic.connectSlots(this);
    connect(addButton,SIGNAL(clicked(bool)),this,SLOT(onAddButton()));
    connect(delButton,SIGNAL(clicked(bool)),this,SLOT(onDelButton()));
    connect(table,SIGNAL(cellClicked(int,int)),this,SLOT(onListClick(int,int)));
    connect(table,SIGNAL(currentCellChanged(int,int,int,int)),this,SLOT(onListMove(int,int,int,int)));

    connect(moveUp,SIGNAL(clicked(bool)),this,SLOT(onMoveUp()));
    connect(moveDown,SIGNAL(clicked(bool)),this,SLOT(onMoveDown()));
    connect(execButton,SIGNAL(clicked(bool)),this,SLOT(onExec()));
    onAddButton();
    onListClick(0,0);
    logic.setConsole(console);
    // readSettings();

}

MainWindow::~MainWindow()
{
    delete ui;
    delete table;
    delete windowsStyle;
}

void MainWindow::onEdit(int row,int column)
{
    logic.onScenarioCellChanged(row,column,table->item(row,column)->text());
}

void MainWindow::onAddButton()
{
    logic.addScenario(ntables,table);
}

void MainWindow::onDelButton()
{
    logic.delScenario(ntables,table,ui->gridLayout);
}

void MainWindow::onListClick(int row,int column)
{
    curRow = logic.listClicked(row,ntables,ui->gridLayout);
}

void MainWindow::onListMove(int row, int column, int pr, int pc)
{
    onListClick(row,column);
}

void MainWindow::onMoveUp()
{
    curRow = logic.moveUp(ntables,table);
}

void MainWindow::onMoveDown()
{
    curRow = logic.moveDown(ntables,table);
}

void MainWindow::onExec()
{
    logic.execAllScripts(ntables,table,qobject_cast<QMainWindow*>(this));
}

void MainWindow::onClearTable()
{
    ntables[curRow]->clearContents();
}

void MainWindow::onClearAll()
{
    table->selectRow(0);
    for(int i=table->rowCount()-1;i>0;i--){
        onDelButton();
    }
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    //writeSettings();
    e->accept();
}

void MainWindow::writeSettings()
{
    logic.writeSettings(ntables,table,this);
}

void MainWindow::readSettings()
{
    //onClearAll();
    logic.readSettings(ntables,table,this);
}
