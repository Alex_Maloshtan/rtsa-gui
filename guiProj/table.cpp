#include "table.h"

ntable::ntable(QWidget *parent) : QWidget(parent)
{
    windowsStyle = QStyleFactory::create("windows");
    table = new QTableWidget(this);
    //table->setGeometry(100,10,700,400);
    table->setRowCount(ROWS);
    table->setColumnCount(COLUMNS);
    //table->setRowHeight(0,100);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->horizontalHeader()->hide();
    table->verticalHeader()->hide();
    table->setStyle(windowsStyle);
    table->setStyleSheet("windows");
    table->setSelectionMode(QAbstractItemView::NoSelection);
    table->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
    this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    table->setContentsMargins(5,0,5,0);
    for (int i=0;i<ROWS;i++){
        for (int j=0;j<COLUMNS;j++){
            QGroupBox * group = new QGroupBox(this);
            grBoxes.push_back(group);
            QBoxLayout* cellItems = new QBoxLayout(QBoxLayout::TopToBottom);
            QTextEdit * input = new QTextEdit();
            input->setFrameShape(QFrame::Box);
            inputs.push_back(input);
            QLabel * label = new QLabel(chipNames[i*COLUMNS+j]);
            labels.push_back(label);
            label->setAlignment(Qt::AlignCenter);

            cellItems->setContentsMargins(5,5,5,5);
            cellItems->setSpacing(0);
            //cellItems->setMargin(0);
            cellItems->addWidget(label);
            QLabel * commentary = new QLabel(chipCommentary[i*COLUMNS+j]);
            label->setFont(QFont("Times",6,QFont::Normal));
            cellItems->addWidget(commentary);
            //input->setText(chipCommentary[i*COLUMNS+j]);
            if(i*COLUMNS+j == 8){
                resetDigital = new QCheckBox("reset");
                connect(resetDigital,SIGNAL(toggled(bool)),this,SLOT(onDigitalResetClick(bool)));
                cellItems->addWidget(resetDigital);
            }
            cellItems->addWidget(input);

            group->setLayout(cellItems);
            label->setFont(QFont("Times",14,QFont::Bold));
            table->setCellWidget(i,j,group);

        }
    }

}

ntable::~ntable()
{
    foreach (QTextEdit* i, inputs) {
        delete i;
    }
    foreach (QLabel* i, labels) {
        delete i;
    }
    foreach (QGroupBox * i, grBoxes) {
        delete i;
    }
    delete table;
    delete windowsStyle;
}

void ntable::writeSettings(int tableNumber,QString saveFileName,QString settingsSaveFileName)
{
    settings = new QSettings(saveFileName,QSettings::IniFormat);
    for (int i=0;i<ROWS;i++){
        for (int j=0;j<COLUMNS;j++){
            settings->setValue("ntable"+QString::number(tableNumber)+"_"+
                                QString::number(i)+"_"+QString::number(j),
                                inputs[i*COLUMNS+j]->document()->toPlainText());
         }
    }
    delete settings;
}

void ntable::readSettings(int tableNumber,QString saveFileName)
{
    settings = new QSettings(saveFileName,QSettings::IniFormat);
    for (int i=0;i<ROWS;i++){
        for (int j=0;j<COLUMNS;j++){
            QString cellText = settings->value("ntable"+QString::number(tableNumber)+"_"+
                                QString::number(i)+"_"+QString::number(j)).toString();
            inputs[i*COLUMNS+j]->setText(cellText);
         }
    }
    delete settings;
}

void ntable::clearContents()
{
    for (int i=0;i<ROWS;i++){
        for (int j=0;j<COLUMNS;j++){
            inputs[i*COLUMNS+j]->setText("");
         }
    }
}

void ntable::resizeEvent(QResizeEvent *event)
{
    table->setGeometry(0,0, event->size().width(),event->size().height());
}

void ntable::onDigitalResetClick(bool checked)
{
    reset = checked;
}
