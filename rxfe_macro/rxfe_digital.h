// ---- Digital control module bit potitions ----
// Macro names correspond to lines on schematics
#define IN_LNA_CTL_BIT 0
#define IN_SW_CTLA_BIT 1
#define IN_SW_CTLB_BIT 2

#define LF_LNA1_CTL_BIT 3

#define LF_BAND_V1_BIT 4
#define LF_BAND_V2_BIT 5

#define HF_LNA_CTL_BIT 6
#define HF_BAND_V1_BIT 7
#define HF_BAND_V2_BIT 8
#define HF_BAND_V3_BIT 9

#define DEM_FLT1_ENBL_BIT 10
#define DEM_FLT2_ENBL_BIT 11

#define DEM_FLT_GAIN_CLR_BIT 12

#define PLL_RX1_CE_BIT 13
#define PLL_RX2_CE_BIT 14

#define DEM_SEL_BIT 15

#define PLL_SW_CTL1_BIT 16

#define ADC_IN_SW_BIT 17

// ---- Select LXM2592 PLL ----
#define PLL_SW_0(x) ( x & (~ (1 << PLL_SW_CTL1_BIT)))
#define PLL_SW_1(x) (x | (1 << PLL_SW_CTL1_BIT))

// ---- Rx HF 0 – 500 MHz Programmable filters ADRF6520 ----
#define DEM_FLT1_SEL(x) ( x | (1 << DEM_SEL_BIT))
#define DEM_FLT2_SEL(x) ( x & (~ (1 << DEM_SEL_BIT)))

#define FLT1_EN(x)      ( x | (1 << DEM_FLT1_ENBL_BIT))
#define FLT1_DISABLE(x) ( x & (~ (1 << DEM_FLT1_ENBL_BIT)))

#define FLT2_EN(x)      ( x | (1 << DEM_FLT2_ENBL_BIT))
#define FLT2_DISABLE(x) ( x & (~ (1 << DEM_FLT2_ENBL_BIT)))

// ---- RX 2xPLL LMX2592 ----
#define PLL_RX1_EN(x)      ( x | (1 << PLL_RX1_CE_BIT))
#define PLL_RX1_DISABLE(x) ( x & (~ (1 << PLL_RX1_CE_BIT)))

#define PLL_RX2_EN(x)      ( x | (1 << PLL_RX2_CE_BIT))
#define PLL_RX2_DISABLE(x) ( x & (~ (1 << PLL_RX2_CE_BIT)))

// ---- DAC LTC2624 CLR line ----
// CLR_N Pulse Width min. 20 ns
// CLR (Pin 11): Asynchronous Clear Input. A logic low at
// this level-triggered input clears all registers and causes
// the DAC voltage outputs to drop to 0V for the LTC2604/
// LTC2614/LTC2624. A logic low at this input sets all registers
// to midscale code and causes the DAC voltage outputs to
// go to midscale for the LTC2604-1/LTC2614-1/LTC2624-1.
// CMOS and TTL compatible.
#define DEM_FLT_GAIN_CLR(x)         ( x & (~ (1 << DEM_FLT_GAIN_CLR_BIT)))
#define DEM_FLT_GAIN_CLR_RELEASE(x) ( x | (1 << DEM_FLT_GAIN_CLR_BIT))

// ---- Rx HF 300 – 6000 MHz Switching filters PE42462 ----
// Table.5, p.8
// LS has an internal 1 MΩ pull-up resistor to logic high
// LS assumed to be float
// LS (1) V3 V2 V1 RFC–RF1 RFC–RF2 RFC–RF3 RFC–RF4 RFC–RF5 RFC–RF6
// 1 1 0 1 ON OFF OFF OFF OFF OFF
// 1 0 0 1 OFF ON OFF OFF OFF OFF
// 1 1 1 0 OFF OFF ON OFF OFF OFF
// 1 0 1 0 OFF OFF OFF ON OFF OFF
// 1 1 0 0 OFF OFF OFF OFF ON OFF
// 1 0 0 0 OFF OFF OFF OFF OFF ON
// X (2) 0 1 1 OFF OFF OFF OFF OFF OFF
#define V1_1(x) (x | (1 << HF_BAND_V1_BIT))
#define V2_1(x) (x | (1 << HF_BAND_V2_BIT))
#define V3_1(x) (x | (1 << HF_BAND_V3_BIT))

#define V1_0(x) (x & (~(1 << HF_BAND_V1_BIT)))
#define V2_0(x) (x & (~(1 << HF_BAND_V2_BIT)))
#define V3_0(x) (x & (~(1 << HF_BAND_V3_BIT)))

#define HF_BAND_ISOLATE(x)  (V3_0(V2_1(V1_1(x))))
#define HF_BAND_Z5(x)       (V3_1(V2_0(V1_1(x))))
#define HF_BAND_Z6(x)       (V3_0(V2_0(V1_1(x))))
#define HF_BAND_Z7(x)       (V3_1(V2_1(V1_0(x))))
#define HF_BAND_Z4(x)       (V3_0(V2_1(V1_0(x))))
#define HF_BAND_Z2(x)       (V3_1(V2_0(V1_0(x))))
#define HF_BAND_Z1(x)       (V3_0(V2_0(V1_0(x))))

// ---- Rx LF 9 kHz – 300 MHz Switching filters PE42540 ---
// Table.5, p.4
// V1 V2
// RF1 on 0 0
// RF2 on 1 0
// RF3 on 0 1
// RF4 on 1 1 (not in use)
#define LF_V1_1(x) (x | (1 << LF_BAND_V1_BIT))
#define LF_V2_1(x) (x | (1 << LF_BAND_V2_BIT))

#define LF_V1_0(x) (x & (~(1 << LF_BAND_V1_BIT)))
#define LF_V2_0(x) (x & (~(1 << LF_BAND_V2_BIT)))

#define LF_BAND_Z10(x)       (LF_V2_0(LF_V1_0(x)))
#define LF_BAND_Z11(x)       (LF_V2_1(LF_V1_0(x)))
#define LF_BAND_Z12(x)       (LF_V2_0(LF_V1_1(x)))

// ---- Rx 9 kHz – 6000 MHz Input LNA AM1065 ----
#define IN_LNA_EN(x)        ( x | (1 << IN_LNA_CTL_BIT))
#define IN_LNA_BYPASS(x)    ( x & (~ (1 << IN_LNA_CTL_BIT)))

// ---- Rx LF 9 kHz – 300 MHz Input LNA AM1065 ----
#define LF_LNA1_EN(x)        ( x | (1 << LF_LNA1_CTL_BIT))
#define LF_LNA1_BYPASS(x)    ( x & (~ (1 << LF_LNA1_CTL_BIT)))

// ---- Rx HF 300 – 6000 MHz Input LNA AM1065 ----
#define HF_LNA_EN(x)        ( x | (1 << HF_LNA_CTL_BIT))
#define HF_LNA_BYPASS(x)    ( x & (~ (1 << HF_LNA_CTL_BIT)))

// ---- Rx 9 kHz – 6000 MHz Input switch HMC641 ----
// Truth Table, p.3
// Control Input   Signal Path State
// A       B       RFC to:
// High    High    RF1
// Low     High    RF2
// High    Low     RF3
// Low     Low     RF4 (not in use)

#define IN_SW_CTLA_1(x) (x | (1 << IN_SW_CTLA_BIT))
#define IN_SW_CTLB_1(x) (x | (1 << IN_SW_CTLB_BIT))

#define IN_SW_CTLA_0(x) (x & (~(1 << IN_SW_CTLA_BIT)))
#define IN_SW_CTLB_0(x) (x & (~(1 << IN_SW_CTLB_BIT)))

#define DC_30_RF_IN(x)    (IN_SW_CTLA_1(IN_SW_CTLB_1(x)))
#define HF_RF_IN(x)       (IN_SW_CTLA_0(IN_SW_CTLB_1(x)))
#define LF_RF_IN(x)       (IN_SW_CTLA_1(IN_SW_CTLB_0(x)))

// ---- ADC Input channel selection ----
#define ADC_IN_SW_0(x) (x & (~(1 << ADC_IN_SW_BIT)))
#define ADC_IN_SW_1(x) (x | (1 << ADC_IN_SW_BIT))



