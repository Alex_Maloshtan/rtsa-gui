#ifndef TABLE_H
#define TABLE_H

#include <QWidget>
#include <QTableWidget>
#define COLUMNS 3
#define ROWS 3
#include <QGroupBox>
#include <QLabel>
#include <QTextEdit>
#include <QBoxLayout>
#include <QHeaderView>
#include <QResizeEvent>
#include <QStyleFactory>
#include <qsettings.h>
#include <QCheckBox>

class ntable : public QWidget
{
    Q_OBJECT
public:
    explicit ntable(QWidget *parent = 0);
    ~ntable();
    QVector<QTextEdit*> inputs;
    void writeSettings(int tableNumber,QString saveFileName,QString settingsSaveFileName);
    void readSettings(int tableNumber, QString saveFileName);
    void clearContents();
    bool reset = false;
signals:

public slots:
private:

    const QString chipNames[10] = {"ADFR6520","ADS5562","HMC625","LXM2592_1","LXM2592_2","LTC2624","LTC5586","PE43704","DIGITAL","Reserved"};
    const QString chipCommentary[10] = {"OR logic",
                                        "Applied to 0x00000000",
                                        "AND logic",
                                        "Applied to 0x00000000. For the same register use |",
                                        "Applied to 0x00000000. For the same register use |",
                                        "Applied to 0x00000000",
                                        "Applied to 0x00000000. For the same register use |",
                                        "Single command with parameter in dB",
                                        "Applied to current control word",
                                        ""};
    QStyle * windowsStyle;
    QSettings * settings;
    QVector<QLabel*> labels;
    QVector<QGroupBox *> grBoxes;
    QTableWidget * table;
    QCheckBox * resetDigital;
    void resizeEvent(QResizeEvent * event);
private slots:
    void onDigitalResetClick(bool checked);
};

#endif // TABLE_H
