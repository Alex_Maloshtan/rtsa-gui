// ------------------------------------------------------------------
// ---- HMC625 ----
// The HMC625BLP5E is a digitally controlled variable
// gain amplifier which operates from DC to 5 GHz, and
// can be programmed to provide anywhere from 13.5
// dB attenuation, to 18 dB of gain, in 0.5 dB steps.
// Any combination of the below states will provide a reduction in
// gain approximately equal to the sum of the bits selected.
// For example: GAIN_M4DB & GAIN_M8DB
#define GAIN_0DB     ( 0x3F )
#define GAIN_M05DB   ( 0x3E )
#define GAIN_M1DB    ( 0x3D )
#define GAIN_M2DB    ( 0x3B )
#define GAIN_M4DB    ( 0x37 )
#define GAIN_M8DB    ( 0x2F )
#define GAIN_M16DB   ( 0x1F )
#define GAIN_M31P5DB ( 0x00 )
