// ------------------------------------------------------------------
// ---- ADFR6520 ----
// Table 5, p. 24
// Usage: word = ENABLE | DC_OFFSET_DISABLE | FR_720MHZ;
#define ENABLE     ((0<<23) | (1 << 12) | ( 1 << 7))
#define DISABLE    ((0<<23) | (1 << 12) )

#define DC_OFFSET_ENABLE     ((0<<23) | (1 << 12) | ( 1 << 4))
#define DC_OFFSET_DISABLE    ((0<<23) | (1 << 12) )

#define FR_36MHZ    ((0<<23) | (1 << 12) | ( 0 ))
#define FR_72MHZ    ((0<<23) | (1 << 12) | ( 1 ))
#define FR_144MHZ   ((0<<23) | (1 << 12) | ( 2 ))
#define FR_288MHZ   ((0<<23) | (1 << 12) | ( 3 ))
#define FR_432MHZ   ((0<<23) | (1 << 12) | ( 4 ))
#define FR_576MHZ   ((0<<23) | (1 << 12) | ( 5 ))
#define FR_720MHZ   ((0<<23) | (1 << 12) | ( 6 ))
#define FR_BYPASS   ((0<<23) | (1 << 12) | ( 7 ))
