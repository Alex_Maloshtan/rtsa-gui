// ------------------------------------------------------------------
// ---- ADS5562 control ----
// Macroses "Rxx_<name>" corresponds to the same register
//
// Usage examples:
// uint32_t word = CLKOUT_POSN_MODE0;

// REG 0x5D
// Low-Frequency Noise Suppression
// 0 Disable low-frequency noise suppression
// 1 Enable low-frequency noise suppression
#define LF_NOISE_SUPPRESSION_EN      ( (0x5D<<8) | 1)
#define LF_NOISE_SUPPRESSION_DISABLE ( (0x5D<<8) )

// REG 0x61
// Output Clock Position Programmability
// 00000    Register value after reset (corresponds to default CLKOUT position)
//          Setup/hold timings with this clock position are specified in the Timing Characteristics for
//          LVDS and CMOS Modes table.
// 00001    Default CLKOUT position.
//          Setup and hold timings with this clock position are specified in the Timing Characteristics
//          for LVDS and CMOS Modes table.
// XX011    CMOS - Rising edge earlier by (3/36) Ts
//          LVDS - Falling edge later by (3/36) Ts
// XX101    CMOS - Rising edge later by (3/36) Ts
//          LVDS - Falling edge later by (6/36) Ts
// XX111    CMOS - Rising edge later by (5/36) Ts
//          LVDS - Falling edge later by (6/36) Ts
// 01XX1    CMOS - Falling edge earlier by (3/36) Ts
//          LVDS - Rising edge later by (3/36) Ts
// 10XX1    CMOS - Falling edge later by (3/36) Ts
//          LVDS - Rising edge later by (7/36) Ts
// 11XX1    CMOS - Falling edge later by (5/36) Ts
//          LVDS - Rising edge later by (7/36) Ts
#define CLKOUT_POSN_MODE0 ( (0x61<<8) | 0)
#define CLKOUT_POSN_MODE1 ( (0x61<<8) | 1)
#define CLKOUT_POSN_MODE2 ( (0x61<<8) | 3)
#define CLKOUT_POSN_MODE3 ( (0x61<<8) | 5)
#define CLKOUT_POSN_MODE4 ( (0x61<<8) | 7)
#define CLKOUT_POSN_MODE5 ( (0x61<<8) | 9)
#define CLKOUT_POSN_MODE6 ( (0x61<<8) | 17)
#define CLKOUT_POSN_MODE1 ( (0x61<<8) | 25)

// REG 0x63
// uint32_t word = R63_SPEED_LOW_M | R63_DF_2S_OFFSETBINARY_M | R63_NORMAL_OPERATION_M;

// Low Sampling Frequency Operation
// 0 DEFAULT SPEED mode (for Fs > 25 MSPS)
// 1 LOW SPEED mode eabled (for Fs ≤ 25 MSPS)
#define R63_SPEED_DEFAULT_M     (0x63<<8)
#define R63_SPEED_LOW_M         ( (0x63<<8) | 1)

// Output Data Format
// 0 2s-complement
// 1 Offset binary
#define R63_DF_2S_COMPLEMENT_M      (0x63<<8)
#define R63_DF_2S_OFFSETBINARY_M    ( (0x63<<8) | (1 << 3) )

// Global STANDBY
// 0 Normal operation
// 1 Global power-down (includes ADC, internal references and output buffers)
#define R63_NORMAL_OPERATION_M      (0x63<<8)
#define R63_POWER_DOWN_M            ( (0x63<<8) | (1 << 7) )

// REG 0x65
// Outputs selected test pattern on data lines
// 000 Normal operation
// 001 All 0s
// 010 All 1s
// 011 Toggle pattern - alternate 1s and 0s on each data output and across data outputs
// 100 Ramp pattern - Output data ramps from 0x0000 to 0xFFFF by one code every clock cycle
// 101 Custom pattern - Outputs the custom pattern in CUSTOM PATTERN registers A and B
// 111 Unused
#define TEST_PATTERN_NORMAL ((0x65<<8) | (0 << 5))
#define TEST_PATTERN_ALL0   ((0x65<<8) | (1 << 5))
#define TEST_PATTERN_ALL1   ((0x65<<8) | (2 << 5))
#define TEST_PATTERN_TOGGLE ((0x65<<8) | (3 << 5))
#define TEST_PATTERN_RAMP   ((0x65<<8) | (4 << 5))
#define TEST_PATTERN_CUSTOM ((0x65<<8) | (5 << 5))
#define TEST_PATTERN_UNUSED ((0x65<<8) | (7 << 5))

// REG 0x68
// Programmable Fine Gain
// 0XXX 1 dB
// 1000 0 dB
// 1001 1 dB, default register value after reset
// 1010 2 dB
// 1011 3 dB
// 1100 4 dB
// 1101 5 dB
// 1110 6 dB
#define GAIN_0DB ( (0x68<<8) | 8)
#define GAIN_1DB ( (0x68<<8) | 9)
#define GAIN_2DB ( (0x68<<8) | 10)
#define GAIN_3DB ( (0x68<<8) | 11)
#define GAIN_4DB ( (0x68<<8) | 12)
#define GAIN_5DB ( (0x68<<8) | 13)
#define GAIN_6DB ( (0x68<<8) | 14)

// REG 0x69
// Custom pattern (D7–D0)
// Program bits D7 to D0 of custom pattern
#define CUSTOM_LOW(x) ( (0x69<<8) | (x & 0xFF))

// REG 0x6A
// Custom pattern (D15–D8)
// Program bits D15 to D8 of custom pattern
#define CUSTOM_HIGH(x) ( (0x6A<<8) | (x & 0xFF))

// REG 0x6C
// Output Interface
// 00 default after reset, state of DFS pin determines interface type. See Table 7.
// 01 DDR LVDS outputs, independent of state of DFS pin.
// 11 Parallel CMOS outputs, independent of state of DFS pin.
#define OI_DEFAULT  ( (0x6C<<8) | (0 << 3))
#define OI_LVDS     ( (0x6C<<8) | (1 << 3))
#define OI_CMOS     ( (0x6C<<8) | (2 << 3))

// REG 0x6D
// Reference
// 0 Internal reference
// 1 External reference mode, force voltage on VCM to set reference.
#define REFERENCE_INTERNAL  ( (0x6D<<8) | (0 << 4))
#define REFERENCE_EXTERNAL  ( (0x6D<<8) | (1 << 4))

// REG 0x6E
// Software resets the ADC
// 1 Resets all registers to default values
#define SW_RESET  ( (0x6E<<8) | (1 << 0))

// REG 0x7E
// uint32_t word = LVDS_CP_4MA5_N | LVDS_CLKTERM_125_N | LVDS_DATATERM_100_N;
// LVDS Buffer Current Programmability
// 00 3.5 mA, default
// 01 2.5 mA
// 10 4.5 mA
// 11 1.75 mA
#define R7E_LVDS_CP_3MA5_N   ( (0x7E<<8) | (0 << 0))
#define R7E_LVDS_CP_2MA5_N   ( (0x7E<<8) | (1 << 0))
#define R7E_LVDS_CP_4MA5_N   ( (0x7E<<8) | (2 << 0))
#define R7E_LVDS_CP_1MA75_N  ( (0x7E<<8) | (3 << 0))

// LVDS Buffer Internal Termination
// 000 No internal termination
// 001 325
// 010 200
// 011 125
// 100 170
// 101 120
// 110 100
// 111 75
#define R7E_LVDS_CLKTERM_NO_N   ( (0x7E<<8) | (0 << 2))
#define R7E_LVDS_CLKTERM_325_N  ( (0x7E<<8) | (1 << 2))
#define R7E_LVDS_CLKTERM_200_N  ( (0x7E<<8) | (2 << 2))
#define R7E_LVDS_CLKTERM_125_N  ( (0x7E<<8) | (3 << 2))
#define R7E_LVDS_CLKTERM_170_N  ( (0x7E<<8) | (4 << 2))
#define R7E_LVDS_CLKTERM_120_N  ( (0x7E<<8) | (5 << 2))
#define R7E_LVDS_CLKTERM_100_N  ( (0x7E<<8) | (6 << 2))
#define R7E_LVDS_CLKTERM_75_N   ( (0x7E<<8) | (7 << 2))

// LVDS Buffer Internal Termination
// 000 No internal termination
// 001 325
// 010 200
// 011 125
// 100 170
// 101 120
// 110 100
// 111 75
#define R7E_LVDS_DATATERM_NO_N   ( (0x7E<<8) | (0 << 5))
#define R7E_LVDS_DATATERM_325_N  ( (0x7E<<8) | (1 << 5))
#define R7E_LVDS_DATATERM_200_N  ( (0x7E<<8) | (2 << 5))
#define R7E_LVDS_DATATERM_125_N  ( (0x7E<<8) | (3 << 5))
#define R7E_LVDS_DATATERM_170_N  ( (0x7E<<8) | (4 << 5))
#define R7E_LVDS_DATATERM_120_N  ( (0x7E<<8) | (5 << 5))
#define R7E_LVDS_DATATERM_100_N  ( (0x7E<<8) | (6 << 5))
#define R7E_LVDS_DATATERM_75_N   ( (0x7E<<8) | (7 << 5))

// REG 0x7F
// LVDS Buffer Internal Termination
// 00 Value specified by <LVDS CURR>
// 01 2x data, 2x clockout currents
// 10 1x data, 2x clockout currents
// 11 2x data, 4x clockout currents
#define LVDS_CURR_DOUBLE_DEF  ( (0x7F<<8) | (0 << 6))
#define LVDS_CURR_DOUBLE_22   ( (0x7F<<8) | (1 << 6))
#define LVDS_CURR_DOUBLE_12   ( (0x7F<<8) | (2 << 6))
#define LVDS_CURR_DOUBLE_24   ( (0x7F<<8) | (3 << 6))
