#include "interfacelogic.h"

interfaceLogic::interfaceLogic()
{
    eng.installExtensions(QJSEngine::ConsoleExtension);
//    QJSValue val = eng.newQObject(&core);
//    eng.globalObject().setProperty("core",val);
    for (int i=0;i<9;i++){
        QVector<uint32_t> a;
        ctrlWords.push_back(a);
    }

    for (int i=0;i<8;i++){
        readFromFile(fileNames[i]);
    }
}

void interfaceLogic::execScript(QString text, const int chip,const bool reset,QString modeName)
{
    uint32_t word = -1;
    QStringList evaluateCmds = text.split("\n");
    if (text.isEmpty()){
        return;
    }

    if (chip == LXM2592_1 || chip == LXM2592_2 || chip == LTC5586 || chip == ADS5562 || chip == PE43704 || chip== LTC2624){
        foreach (QString evalText, evaluateCmds) {
            if (evalText.isEmpty()) {
                continue;
            }
            QJSValue js_value = eng.evaluate(evalText);
            if(js_value.isError()) {
                guiConsole->append(QString("%1: %2: %3: %4")
                               .arg(modeName)
                               .arg(chipNames[chip])
                               .arg(js_value.property("lineNumber").toString())
                               .arg(js_value.property("message").toString()).toUtf8().constData()
                                   );
            }
            if (js_value.isNumber()) {
                word = js_value.toNumber();
                word &= 0xFFFFFFFF;
            }
            ctrlWords[chip].push_back(word);
//            qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalText)
//                                                .arg(QString::number(word,16))
//                                                .arg(QString::number(word,2))
//                                                .arg(chipNames[chip]);
        }
    }

    if (chip == ADFR6520) {
        QString evalOr;
        foreach (QString evalText, evaluateCmds) {
            evalOr.append(evalText);
            evalOr.append(" |");
        }
        evalOr.chop(1);
        QJSValue js_value = eng.evaluate(evalOr);
                if(js_value.isError()){
                    guiConsole->append(QString("%1: %2: %3: %4")
                                   .arg(modeName)
                                   .arg(chipNames[chip])
                                   .arg(js_value.property("lineNumber").toString())
                                   .arg(js_value.property("message").toString()).toUtf8().constData()
                                       );
                }
        if (js_value.isNumber()) {
             word = js_value.toNumber();
        }
        ctrlWords[chip].push_back(word);
//        qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalOr)
//                                            .arg(QString::number(word,16))
//                                            .arg(QString::number(word,2))
//                                            .arg(chipNames[chip]);
    }

    if (chip== HMC625) {
        QString evalAnd;
        foreach (QString evalText, evaluateCmds) {
            evalAnd.append(evalText);
            evalAnd.append(" &");
        }
        evalAnd.chop(1);
        QJSValue js_value = eng.evaluate(evalAnd);
        if(js_value.isError()){
            guiConsole->append(QString("%1: %2: %3: %4")
                           .arg(modeName)
                           .arg(chipNames[chip])
                           .arg(js_value.property("lineNumber").toString())
                           .arg(js_value.property("message").toString()).toUtf8().constData()
                               );
        }
        if (js_value.isNumber()) {
             word = js_value.toNumber();
        }
        ctrlWords[chip].push_back(word);
//        qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalAnd)
//                                            .arg(QString::number(word,16))
//                                            .arg(QString::number(word,2))
//                                            .arg(chipNames[chip]);
    }

    if(chip== DIGITAL) {
        if (reset || initDigitalInputWord){
        eng.evaluate("inputWord = 0");
        initDigitalInputWord = false;
        }
        foreach (QString evalText, evaluateCmds) {
            QJSValue js_value = eng.evaluate("inputWord = "+ evalText +"(inputWord)");
                    if(js_value.isError()){
                        guiConsole->append(QString("%1: %2: %3: %4")
                                       .arg(modeName)
                                       .arg(chipNames[chip])
                                       .arg(js_value.property("lineNumber").toString())
                                       .arg(js_value.property("message").toString()).toUtf8().constData()
                                           );
                    }
                    js_value = eng.evaluate("inputWord");
                    if (js_value.isNumber()) {
                        word = js_value.toNumber();
                    }
//                    qDebug() << QString("%1 0x%2 %3 IC: %4").arg(evalText)
//                                                        .arg(QString::number(word,16))
//                                                        .arg(QString::number(word,2))
//                                                        .arg(chipNames[chip]);
        }
        ctrlWords[chip].push_back(word);
    }


}

void interfaceLogic::addScenario(QVector<ntable *> & ntables, QTableWidget * scenarioList)
{
    ntable * table= new ntable();
    ntables.push_back(table);
    const int lastRow = scenarioList->rowCount();
    scenarioList->insertRow(lastRow);
    table->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    for (int i=0;i<scenarioList->columnCount();i++){
        scenarioList->setItem(lastRow,i,new QTableWidgetItem());
    }
    scenarioList->item(lastRow,0)->setText("Mode"+QString::number(scenarioList->rowCount()));
    jumpTo.push_back(scenarioList->item(lastRow,2)->text().toInt()-1);
    if (!scenarioList->selectedItems().isEmpty()){
        const int selectedRow = scenarioList->row(scenarioList->selectedItems().at(0));
        scenarioList->selectRow(scenarioList->rowCount()-1);
        for (int i=0;i<lastRow-selectedRow-1;i++){
            moveUp(ntables,scenarioList);
        }
    }

}

void interfaceLogic::delScenario(QVector<ntable *> &ntables, QTableWidget *scenarioList, QGridLayout *gridLayout)
{
    if(scenarioList->rowCount() == 1) return;
    if (scenarioList->selectedItems().isEmpty()) return;
    const int row = scenarioList->row(scenarioList->selectedItems().at(0));
    QLayoutItem * child= gridLayout->takeAt(7);
    if (child){
    child->widget()->hide();
    gridLayout->removeWidget(child->widget());
    gridLayout->removeItem(child);
    delete child;
    }
    scenarioList->removeRow(row);
    delete ntables[row];
    ntables.remove(row);
    for (int i=row;i<scenarioList->rowCount();i++){
        int branch = scenarioList->item(i,2)->text().toInt();
        if (branch >= row+1){
            branch--;
        }
        if (branch <= 0 || branch > scenarioList->rowCount()){
            scenarioList->item(i,2)->setText("не опред.");
        } else {
            scenarioList->item(i,2)->setText(QString::number(branch));
        }


    }
}

int interfaceLogic::moveUp(QVector<ntable *> &ntables, QTableWidget *scenarioList)
{
    const int sourceRow = scenarioList->row(scenarioList->selectedItems().at(0));
    const int destRow = sourceRow-1;
    if (destRow < 0) return 0;
    for (int i=0;i<scenarioList->columnCount();i++){
        QTableWidgetItem * src = scenarioList->item(sourceRow,i)->clone();
        QTableWidgetItem * dst = scenarioList->item(destRow,i)->clone();
        //delete table->item(sourceRow,i)
        scenarioList->setItem(sourceRow,i,dst);
        scenarioList->setItem(destRow,i,src);
    }
    int jumpSwap = jumpTo[sourceRow];
    jumpTo[sourceRow] = jumpTo[destRow];
    jumpTo[destRow] = jumpSwap;
    ntable * temp = ntables.at(sourceRow);
    ntables.replace(sourceRow,ntables.at(destRow));
    ntables.replace(destRow,temp);
    scenarioList->selectRow(destRow);
    return destRow;
}

int interfaceLogic::moveDown(QVector<ntable *> &ntables, QTableWidget *scenarioList)
{
    const int sourceRow = scenarioList->row(scenarioList->selectedItems().at(0));
    const int destRow = sourceRow+1;
    if (destRow >= scenarioList->rowCount()) return scenarioList->rowCount() - 1;
    for (int i=0;i<scenarioList->columnCount();i++){
        QTableWidgetItem * src = scenarioList->item(sourceRow,i)->clone();
        QTableWidgetItem * dst = scenarioList->item(destRow,i)->clone();
        delete scenarioList->item(sourceRow,i);
        delete scenarioList->item(destRow,i);
        scenarioList->setItem(sourceRow,i,dst);
        scenarioList->setItem(destRow,i,src);
    }
    int jumpSwap = jumpTo[sourceRow];
    jumpTo[sourceRow] = jumpTo[destRow];
    jumpTo[destRow] = jumpSwap;
    ntable * temp = ntables.at(sourceRow);
    ntables.replace(sourceRow,ntables.at(destRow));
    ntables.replace(destRow,temp);
    scenarioList->selectRow(destRow);
    return destRow;
}
void interfaceLogic::moveFile(QString path,QString to)
{
    QFile save(path+"/1/1.ini");
    save.rename(to+"_");
    //QDir folder(path);
    //folder.removeRecursively();
    QFile save2(to+"_");
    save2.rename(to);
}
void interfaceLogic::writeSettings(QVector<ntable *> &ntables, QTableWidget *table, QMainWindow *window)
{  
   // QString file = QFileDialog::getSaveFileName(window,"");

    QFileDialog fd(window,"Save scenario");
   // fd.setWindowFlags(Qt::Popup);
    fd.setFileMode(QFileDialog::AnyFile);
    fd.setWindowModality(Qt::ApplicationModal);
    fd.setWindowState(Qt::WindowActive);

    fd.setAcceptMode(QFileDialog::AcceptSave);
    fd.setFocusPolicy(Qt::StrongFocus);
    //fd.grabKeyboard();
    //fd.setAttribute(Qt::WA_);
    fd.setModal(true);
    fd.exec();
    //fd.setFocus();
    //fd.releaseKeyboard();
    if (fd.selectedFiles().isEmpty()){
        return;
    }
    QString saveFileName = fd.selectedFiles().first();

    QSettings::setDefaultFormat(QSettings::IniFormat);
    QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,
                     saveFileName);
    settings = new QSettings(saveFileName,QSettings::IniFormat);
    QString settingsSaveFileName = settings->fileName();

    QString debug = settings->fileName();
    settings->setValue("tabsNum",table->rowCount()-1);//1 уже создаем по-умолчанию
    settings->setValue("tabsColumns",table->columnCount());
    for(int i=0;i<table->rowCount();i++){
        for(int j=0;j<table->columnCount();j++){
            settings->setValue("cell"+QString::number(i)+"_"+QString::number(j),table->item(i,j)->text());
        }
        ntables[i]->writeSettings(i,saveFileName,settingsSaveFileName);
    }
    if (table->selectedItems().isEmpty()){
        settings->setValue("currentTabRow",0);
    } else {
        settings->setValue("currentTabRow",table->row(table->selectedItems().at(0)));
    }
    delete settings;
    moveFile(settingsSaveFileName,saveFileName);
}
void interfaceLogic::moveFileForQSettings(QString path, QString to)
{
    QFile save2(to);
    save2.rename(to+"/1/1.ini");
}

void interfaceLogic::addDelay(QString text)
{
    QRegExp delayTextRx("(DELAY|delay)[(]\\d+[)]");
    delayTextRx.setCaseSensitivity(Qt::CaseInsensitive);
    if (delayTextRx.indexIn(text) != -1){
        QString delayText = text.mid(delayTextRx.indexIn(text),delayTextRx.matchedLength());
        delayText.remove(0,6);
        delayText.chop(1);
        delays.push_back(delayText.toInt());
    } else {
        delays.push_back(0);
    }
}

void interfaceLogic::addTrigger(QString text)
{
    QRegExp triggerTextRx("trigger)");
    triggerTextRx.setCaseSensitivity(Qt::CaseInsensitive);
    if (triggerTextRx.indexIn(text) != -1){
//        QString triggerText = text.mid(triggerTextRx.indexIn(text),triggerTextRx.matchedLength());
//        triggerText.remove(0,8);
//        triggerText.chop(1);
        triggers.push_back(TRIGGER_EN);
    } else {
        triggers.push_back(NO_TRIGGER);
    }
}
void interfaceLogic::readSettings(QVector<ntable *> &ntables, QTableWidget *table, QMainWindow *window)
{
    QCoreApplication::setOrganizationName("1");
    QCoreApplication::setApplicationName("1");
    QFileDialog fd;
    QString saveFileName = fd.getOpenFileName(window,"Load scenario");
    //QString saveFileName = QFileDialog::getOpenFileName(table,"Load scenario");

    if (saveFileName.isNull()){
        return;
    }
    table->selectRow(0);
    for(int i=table->rowCount()-1;i>0;i--){
        delScenario(ntables,table,layoutPtr);
    }

    QSettings::setDefaultFormat(QSettings::IniFormat);
    QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,
                     saveFileName);
    settings = new QSettings(saveFileName,QSettings::IniFormat);
    QString settingsSaveFileName = settings->fileName();
    moveFileForQSettings(saveFileName,settingsSaveFileName);
    int tabsNum,tabsColumns;
    tabsNum = settings->value("tabsNum").toInt();
    tabsColumns = settings->value("tabsColumns").toInt();
    jumpTo.reserve(tabsNum);
    for (int i=0;i<tabsNum;i++){
        addScenario(ntables,table);
    }
    for(int i=0;i<table->rowCount();i++){
        for(int j=0;j<table->columnCount();j++){
            QString cellText = settings->value("cell"+QString::number(i)+"_"+QString::number(j)).toString();
            table->item(i,j)->setText(cellText);
        }
        ntables[i]->readSettings(i,settingsSaveFileName);
    }
    table->selectRow(settings->value("currentTabRow").toInt());
    delete settings;
}

QVector<int> interfaceLogic::parseMux(QTableWidget *scenarioTable)
{
    QVector<int> muxMode;
    for(int i=0;i<scenarioTable->rowCount();i++){
        QRegExp mux1("(MUX1|mux1)");
        mux1.setCaseSensitivity(Qt::CaseInsensitive);
        QRegExp mux2("(MUX2|mux2)");
        mux2.setCaseSensitivity(Qt::CaseInsensitive);
        QString muxText = scenarioTable->item(i,1)->text();
        const int mux1Index = mux1.indexIn(muxText);
        const int mux2Index = mux2.indexIn(muxText);
        if (mux1Index != -1 && mux2Index != -1){
            muxMode.push_back(MUX1_2);
        }
        if (mux1Index == -1 && mux2Index == -1){
            muxMode.push_back(NOMUX);
        }
        if (mux1Index == -1 && mux2Index != -1){
            muxMode.push_back(MUX2);
        }
        if (mux1Index != -1 && mux2Index == -1){
            muxMode.push_back(MUX1);
        }
    }
    return muxMode;
}

int interfaceLogic::strToSizeInBytes(QString sizeStr)
{
    const int multiplier = KB_1024; //1000 или 1024
    double size = 0.0;
    QRegExp kilo("\\d+\\.?\\d*\\s*(K|k)");
    QRegExp mega("\\d+\\.?\\d*\\s*(M|m)");
    QRegExp giga("\\d+\\.?\\d*\\s*(G|g)");
    QRegExp bytes("\\d+\\s*$");
    if(giga.indexIn(sizeStr) != -1){
        QString part = sizeStr.mid(giga.indexIn(sizeStr),giga.matchedLength()-1);
        bool ok;
        double partSize = part.toDouble(&ok);
        if (ok){
            size += partSize * multiplier * multiplier * multiplier;
        }
    }
    if(mega.indexIn(sizeStr) != -1){
        int debugIndex = mega.indexIn(sizeStr);
        QString part = sizeStr.mid(debugIndex,mega.matchedLength()-1);
        bool ok;
        double partSize = part.toDouble(&ok);
        if (ok){
            size += partSize * multiplier * multiplier;
        }
    }
    if(kilo.indexIn(sizeStr) != -1){
        QString part = sizeStr.mid(kilo.indexIn(sizeStr),kilo.matchedLength()-1);
        bool ok;
        double partSize = part.toDouble(&ok);
        if (ok){
            size += partSize * multiplier;
        }
    }
    if(bytes.indexIn(sizeStr) != -1){
        QString part = sizeStr.mid(bytes.indexIn(sizeStr),bytes.matchedLength());
        bool ok;
        double partSize = part.toDouble(&ok);
        if (ok){
            size += partSize;
        }
    }
    return (int)size;
}



int interfaceLogic::listClicked(int row ,QVector<ntable *> & ntables,QGridLayout * gridLayout)
{
    QLayoutItem * child= gridLayout->takeAt(7);
    if (child){
    child->widget()->hide();
    gridLayout->removeWidget(child->widget());
    gridLayout->removeItem(child);
    delete child;
    }
    gridLayout->addWidget(ntables[row],3,0,5,5);
    //gridLayout->setColumnStretch(5,14);

    ntables[row]->show();
    return row;
}

void interfaceLogic::setupScenarioList(QTableWidget *table)
{
    table->setColumnCount(5);
    table->setHorizontalHeaderItem(4,new QTableWidgetItem("ADS5562"));
    table->setHorizontalHeaderItem(3,new QTableWidgetItem("ADS54J60"));
    table->setHorizontalHeaderItem(2,new QTableWidgetItem("Branch"));
    table->setHorizontalHeaderItem(1,new QTableWidgetItem("PLL LOCK"));
    table->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
    //table->verticalHeader()->hide();
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void interfaceLogic::readFromFile(QString fileName)
{
    headerFile.setFileName(fileName);
    if(!headerFile.open(QIODevice::ReadOnly|QIODevice::Text)){
        qWarning() << QString("header file %1 not found!").arg(fileName);
        return;
    }
    QRegExp rx("\\s*#define ");
    QRegExp function("^\\S+[(]\\S+[)]");
    while(!headerFile.atEnd()){
    QString line = headerFile.readLine() ;
    int defPos = rx.indexIn(line);
        if (defPos != -1){
            line.remove(0,8);   //remove define
            if (function.indexIn(line) != -1){
                const int functionLastIndex = function.indexIn(line)+function.matchedLength();
                line.insert(functionLastIndex,"{return ");
                line.chop(1);
                line.append("; }");
                line.insert(0,"function ");
                //is function
            } else {
                int spaceIndex = line.indexOf(' ');
                line.replace(spaceIndex,1,QChar('='));
                line.chop(1);
                line.append(";");
                line.insert(0,"var ");
            }            
//            qDebug() << QString("%1:%2: %3: %4")
//                        .arg(__FILE__)
//                        .arg(__LINE__)
//                        .arg(__FUNCTION__)
//                        .arg(line).toUtf8().constData();

            QJSValue val = eng.evaluate(line);
            if(val.isError()){
                qCritical() << line;
                qCritical() << QString("%1:%2: %3")
                               .arg(headerFile.fileName())
                               .arg(val.property("lineNumber").toString())
                               .arg(val.property("message").toString()).toUtf8().constData();
            }
        }

    }
    headerFile.close();
}

void interfaceLogic::execAllScripts(QVector<ntable *> ntables,QTableWidget * scenarioTable,QMainWindow * window)
{
    // Control words computation for all ICs and modes
    // Store data to QVector<QVector<uint32_t>> ctrlWords;
    // External vector: IC
    // Internal vector: control words

    if(scenarioTable->item(scenarioTable->rowCount()-1,2)->text() == QString("")){
        scenarioTable->item(scenarioTable->rowCount()-1,2)->setText("stop");
    }
    // Cycle over ICs
    QFileDialog fd;
    //fd.overrideWindowState(Qt:);
    fd.setWindowState(Qt::WindowActive);
    QString saveFileName = fd.getSaveFileName(window,"Save binary");
    BinaryFIleGenerator gen(saveFileName);
    for (int i=0;i<9;i++) {
        gen.nextIC(chipNames[i]);
        gen.ICnum = i;
        qDebug() << "  IC " << chipNames[i];
        // Cycle over modes
        int nRows = scenarioTable->rowCount();
        for(int row = 0; row < nRows; row++) {
            //rowsAddrs[row] = addr;
            qDebug() << "    Mode " << QString::number(row);
            // Control words computation
            execScript(ntables[row]->inputs[i]->document()->toPlainText(), i,ntables[row]->reset,
                       scenarioTable->item(row,0)->text());
                       gen.addMode(scenarioTable->item(row,2)->text(),ctrlWords[i]);
            ctrlWords[i].clear();
        }

    }
    muxSettings = parseMux(scenarioTable);
    triggers.clear();
    delays.clear();
    for (int i=0;i<scenarioTable->rowCount();i++){
        addDelay(scenarioTable->item(i,1)->text());
        addTrigger(scenarioTable->item(i,1)->text());
        int ads1bytes = strToSizeInBytes(scenarioTable->item(i,3)->text());
        int ads2bytes = strToSizeInBytes(scenarioTable->item(i,4)->text());
        gen.addSetting(delays.last(),muxSettings[i],triggers.last(),ads1bytes,ads2bytes);
    }
    return;
}
QByteArray interfaceLogic::generateModeSettings(){
    QVector<uint32_t> rowAddrs;
    QByteArray modeSettings;
    rowAddrs.reserve(muxSettings.size());
    uint32_t settingsOffset = lastAddressInFile;
    qDebug() << lastAddressInFile;
    for (int i=0;i<muxSettings.size();i++){
        uint32_t muxAndDelay = muxSettings[i];
        muxAndDelay = muxAndDelay << 30;
        if (triggers[i]==TRIGGER_EN){
            muxAndDelay |= 1 << 29;
        }
        muxAndDelay |= (0x1FFFFFFF & delays[i]);
        modeSettings.append((char*)&muxAndDelay,sizeof(uint32_t));
        uint32_t ads1 = ads1Bytes[i];
        uint32_t ads2 = ads2Bytes[i];
        modeSettings.append((char*)&ads1,sizeof(uint32_t));
        modeSettings.append((char*)&ads2,sizeof(uint32_t));
        uint32_t nextSettingsAddress = settingsOffset + jumpTo[i]*16;
        modeSettings.append((char*)&nextSettingsAddress,sizeof(uint32_t));
    }
    return modeSettings;
}
void interfaceLogic::onScenarioCellChanged(int row, int column, QString value)
{
//    int branchval = value.toInt() - 1;//0 - первый режим
    //    jumpTo[row] = branchval;
}

void interfaceLogic::setConsole(QTextEdit *console_)
{
    guiConsole = console_;
}

void interfaceLogic::setLayoutPtr(QGridLayout *ptr)
{
    layoutPtr = ptr;
}






