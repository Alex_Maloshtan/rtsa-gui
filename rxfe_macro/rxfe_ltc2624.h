// ------------------------------------------------------------------
// ---- LTC2624 ----
// The command (C3-C0) and address (A3-A0) assignments
// are shown in Table 1. The first four commands in the table
// consist of write and update operations. A write operation
// loads a 16-bit data word from the 32-bit shift register
// into the input register of the selected DAC, n. An update
// operation copies the data word from the input register to
// the DAC register. Once copied into the DAC register, the
// data word becomes the active 16-, 14- or 12-bit input
// code, and is converted to an analog voltage at the DAC
// output. The update operation also powers up the selected
// DAC if it had been in power-down mode. The data path
// and registers are shown in the block diagram.
// Table 1, p.11 
#define DAC_A 0
#define DAC_B 1
#define DAC_C 2
#define DAC_D 3
#define DAC_ALL 0xF

#define WRITE(dac,x)       ((0<<20) | (dac << 16) | ((x & 0xFFF) << 4))
#define UPDATE(dac)         ((1<<20) | (dac << 16) )
#define WUALL(dac,x)        ((2<<20) | (dac << 16) | ((x & 0xFFF) << 4))
#define WU(dac,x)           ((3<<20) | (dac << 16) | ((x & 0xFFF) << 4))
#define POWERDOWN(dac)      ((4<20)  | (dac << 16))
