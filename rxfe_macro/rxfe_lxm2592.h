// ---- LXM2592 ----
// Gereral purpose write definition
#define W(x) (x)

// REG 0
#define WR0 ((0<<23) | (0 << 16) | (1 << 9))

// Lock detect enable
// Default: 1
#define R0_LOCK_DETECT_ENABLE   (WR0 | (1 << 13))
#define R0_LOCK_DETECT_DISABLE  (WR0 | (0 << 13))

// Used for when PFD freq is high
// Default: 0
#define PFD_NOT_USED 0
#define PFD_100MHZ 1
#define PFD_150MHZ 2
#define PFD_200MHZ 3
#define R0_FCAL_HPFD_ADJ(x) (WR0 | (x<<7) )

// Used for when PFD freq is low
// Default: 0
#define PFD_5MHZ 1
#define PFD_10MHZ 2
#define PFD_20MHZ 3
#define R0_FCAL_LPFD_ADJ(x) (WR0 | (x<<5) )

// Enable amplitude calibration
// 1: enable (calibration algorithm will set VCO amplitude.
// For manual mode set register VCO_IDAC_OVR=1, and then
// set the VCO amplitude by register VCO_IDAC)
// 0: disable
// Default: 1
#define R0_ACAL_ENABLE   (WR0| (1 << 4) )
#define R0_ACAL_DISABLE  (WR0| (0 << 4) )

// Enable frequency calibration
// 1: enable (writing 1 to this register triggers the calibration sequence)
// 0: disable
// Default: 1
#define R0_FCAL_ENABLE   (WR0 | (1 << 3))
#define R0_FCAL_DISABLE  (WR0 | (0 << 3))

// Signal at MUXOUT pin
// 1: Lock Detect (3.3 V if locked, 0 V if unlocked)
// 0: Readback (3.3-V digital output)
// Default: 1
#define R0_MUXOUT_SEL_LOCK      (WR0 | (1 << 2))
#define R0_MUXOUT_SEL_READBACK  (WR0 | (0 << 2))

// Reset: Write with a value of 1 to reset device (this register will self-switch back to 0)
// Default: 0
#define R0_RESET                (WR0 | (1 << 1))

// Powerdown whole device
// 1: power down
// 0: power up
// Default: 0
#define R0_POWERDOWN            (WR0 | (1 << 0))
#define R0_POWERUP              (WR0 | (0 << 0))

// REG 1
#define WR1 ((0<<23) | (1 << 16) | (1 << 11) | (1 << 3))

// Divides down the OSCin signal for calibration clock
// Calibration Clock = OSCin / 2^CAL_CLK_DIV
// Set this value so that calibration clock is less than but as close
// to 200MHz as possible if fast calibration time is desired.
// Default: 3
#define R1_CALC_CLK_DIV(x)         (WR1 | ( x & 0x07 ))

// REG 4
#define WR4 ((0<<23) | (4 << 16) | (1 << 6) | (1 << 1) | (1 << 0))

// VCO amplitude calibration delay. Lowering this value can speed
// calibration time. The guideline for this register is 2 x
// [ACAL_CMP_DLY value] x [calibration clock period] > 200ns. As
// described in CAL_CLK_DIV, the calibration clock is defined as
// OSCin / 2^CAL_CLK_DIV. For example, with the fastest
// calibration clock of 200MHz (OSCin=200MHz and
// CAL_CLK_DIV=0), the period is 5ns. So ACAL_CMP_DLY
// should be > 20. With the same derivation, an example of a
// OSCin=100MHz, ACAL_CMP_DLY should be > 10. This register
// is left at a default value of 25 if there is no need to shorten
// calibration time.
// Default: 25
#define R4_ACAL_CMP_DLY(x)         (WR4 | (( x & 0xFF ) << 8))

// REG 8
#define WR8 ((0<<23) | (8 << 16) | (1 << 12) | (1 << 7) | (1 << 2))

// This is the over-ride bit for VCO amplitude (or IDAC value).
// When this is enabled, the VCO amplitude calibration function
// (ACAL_EN) is not used. VCO_IDAC register can be programmed
// to set the amplitude. Keep the VCO_IDAC value within 250 and 450.
// Default: 0
#define R8_VCO_IDAC_OVR_ENABLE         (WR8 | (1 << 13))
#define R8_VCO_IDAC_OVR_DISABLE        (WR8 | (0 << 13))

// This is the over-ride bit for VCO capacitor bank code (or
// CAPCTRL value). When this is enabled, the VCO frequency
// calibration function (FCAL_EN) is not used. the VCO_CAPCTRL
// register can be programmed to set the VCO frequency within the
// selected VCO core. The VCO core is selected by setting
// VCO_SEL_FORCE=1 and then selecting the core with
// VCO_SEL=1,2,3,4,5,6, or 7
// Default: 0
#define R8_VCO_CAPCTRL_OVR_ENABLE         (WR8 | (1 << 10))
#define R8_VCO_CAPCTRL_OVR_DISABLE        (WR8 | (0 << 10))

// REG 9
#define WR9 ((0<<23) | (9 << 16) | (1 << 8) | (1 << 1))

// Reference path doubler
// Default: 0
#define R9_OSC_2X_ENABLE         (WR9 | (1 << 11))
#define R9_OSC_2X_DISABLE        (WR9 | (0 << 11))

// Enable reference path
// Default: 1
#define R9_REF_ENABLE         (WR9 | (1 << 9))
#define R9_REF_DISABLE        (WR9 | (0 << 9))

// REG 10
#define WR10 ((0<<23) | (10 << 16) | (1 << 12) | (1 << 6) | (1 << 4) | (1 << 3))

// Input signal path multiplier (input range from 40 - 70 MHz, output range from 180 - 250 MHz)
#define R10_MULT(x)         (WR10 | (( x & 0x1F ) << 7))

// REG 11
#define WR11 ((0<<23) | (1 << 19) | (1 << 17) | (1 << 16) | (1 << 3))

// R divider after multiplier and before PFD
#define R11_PLL_R(x)         (WR11 | (( x & 0xFF ) << 4))

// REG 12
#define WR12 ((0<<23) | (12 << 16) | (1 << 14) | (1 << 13) | (1 << 12))

// R divider after OSCin doubler and before multiplier
#define R12_PLL_R_PRE(x)         (WR12 | (( x & 0xFFF ) << 0))

// REG 13
#define WR13 ((0<<23) | (13 << 16))

// Enable charge pump
#define R13_CP_EN         (WR13 | (1 << 14))

// PFD mode
// 0: Dual PFD (default)
// 3: Single PFD (ONLY use if PFD freq is higher than 200MHz)
#define R13_PFD_CTL_DUAL          (WR13 | (0 << 8))
#define R13_PFD_CTL_SINGLE        (WR13 | (3 << 8))

// REG 14
#define WR14 ((0<<23) | (14 << 16))
// Charge pump current (DN) – must equal to charge pump current
// (UP). Can activate any combination of bits.
// <bit 4>: 1.25 mA
// <bit 3>: 2.5 mA
// <bit 2>: 0.625 mA
// <bit 1>: 0.312 mA
// <bit 0>: 0.156 mA

#define R14_CPUMPC_1P25  (1 << 4)
#define R14_CPUMPC_2P5   (1 << 3)
#define R14_CPUMPC_0P625 (1 << 2)
#define R14_CPUMPC_0P312 (1 << 1)
#define R14_CPUMPC_0P156 (1 << 0)

// Charge pump current (DN) – must equal to charge pump current
// (UP). Can activate any combination of bits.
#define R14_CPUMPC(x) (WR14 | (x<<7) | (x<<2))

// Charge pump gain multiplier - multiplies charge pump current by
// a given factor:
// 3: multiply by 2.5
// 2: multiply by 1.5
// 1: multiply by 2
// 0: no multiplication
#define R14_CP_ICOARSE_X1   (WR14 | 0)
#define R14_CP_ICOARSE_X2   (WR14 | 1)
#define R14_CP_ICOARSE_X1P5 (WR14 | 2)
#define R14_CP_ICOARSE_X2P5 (WR14 | 3)

// REG 19
#define WR19 ((0<<23) | (19 << 16) | (1 << 2) | (1 << 0))

// This is the VCO amplitude (or IDAC value). When VCO_IDAC is
// over-riden with VCO_IDAC_OVR=1, VCO amplitude calibration
// function (ACAL_EN) is not used. VCO_IDAC register can be
// programmed to set the amplitude. VCO_IDAC value must be
// kept within 250 and 450.
// Default: 300
#define R19_VCO_IDAC(x) (WR19 | ((x &0x1FF) << 3) )

// REG 20
#define WR20 ((0<<23) | (20 << 16))

// This register is used to aid the VCO amplitude calibration
// function (ACAL_EN). By default the amplitude calibration
// function searches from the low end of VCO_IDAC until it
// reaches the target value. Like the VCO_IDAC, this must be kept
// within 250 and 450. This can be set to a value closer to the
// target value, then the amplitude calibration time can be
// shortened typically final VCO_IDAC is somewhere around 300.
// Default: 300
#define R20_ACAL_VCO_IDAC_STRT(x) (WR20 | ((x &0x1FF) << 0) )

// REG 22
#define WR22 ((0<<23) | (22 << 16) | (1 << 13) | (1 << 9) | (1 << 8))

// This is the VCO capacitor bank code (or CAPCTRL value).
// When VCO_CAPCTRL is over-riden with
// VCO_CAPCTRL_OVR=1, VCO frequency calibration function
// (FCAL_EN) is not used. VCO_CAPCTRL register can be
// programmed to set the frequency in that core.
// VCO_SEL_FORCE=1 has to be set and VCO_SEL to select the
// VCO core, then CAPCTRL values between 0 to 183 will produce
// frequencies within this core (0 being the highest frequency and
// 183 the lowest).
// Default: 0
#define R22_ACAL_VCO_CAPCTRL(x) (WR22 | ((x &0xFF) << 0) )

// REG 23
#define WR23 ((0<<23) | (23 << 16) | (1 << 15) | (1 << 6) | (1 << 1))

// This is a register that aids the frequency calibration function.
// When this is enabled, a VCO core can be selected for the
// frequency calibration to start at, set by register VCO_SEL. By
// default the frequency calibration starts from VCO core 7 and
// works its way down. If you want for example to lock to a
// frequency in VCO core 1, you can set VCO_SEL to 2, so the
// calibration will start at VCO core 2 and end at target frequency
// at VCO core 1 faster.
// Default: 0
#define R23_FCAL_VCO_SEL_STRT_ENABLE  (WR23 | (1 << 14))
#define R23_FCAL_VCO_SEL_STRT_DISABLE (WR23 | (0 << 14))

// This is the register used to select VCO cores. It works for
// VCO_CAPCTRL when VCO_CAPCTRL_OVR=1 and
// VCO_SEL_FORCE=1. It also aids the frequency calibration
// function with FCAL_VCO_SEL_STRT.
// Default: 1
#define R23_VCO_SEL(x) (WR23 | ((x &0x07) << 11))

// This register works to force selection of VCO cores. If
// VCO_CAPTRL_OVR=1 and this register is enabled, you can
// select the VCO core to use with VCO_SEL.
// Default: 0
#define R23_VCO_SEL_FORCE_ENABLE  (WR23 | (1 << 10))
#define R23_VCO_SEL_FORCE_DISABLE (WR23 | (0 << 10))

// REG 30
#define WR30 ((0<<23) | (30 << 16) | (1 << 5) | (1 << 4) | (1 << 2))

// MASH dithering: toggle on/off to randomize
// Default: 0
#define R30_MASH_DITHER_ON  (WR30 | (1 << 10))
#define R30_MASH_DITHER_OFF (WR30 | (0 << 10))

// Enable VCO doubler
// Default: 0
#define R30_VCO_2X_ENABLE  (WR30 | (1 << 0))
#define R30_VCO_2X_DISABLE (WR30 | (0 << 0))

// REG 31
#define WR31 ((0<<23) | (31 << 16) | (1 << 0))

// Power down buffer between VCO and output B
// Default: 1
#define R31_VCO_DISTB_PD  (WR31 | (1 << 10))
#define R31_VCO_DISTB_PU  (WR31 | (0 << 10))

// Power down buffer between VCO and output A
// Default: 0
#define R31_VCO_DISTA_PD  (WR31 | (1 << 9))
#define R31_VCO_DISTA_PU  (WR31 | (0 << 9))

// Power down buffer between VCO and channel divider
// ???
// Default: 0
#define R31_CHDIV_DIST_PD  (WR31 | (1 << 7))
#define R31_CHDIV_DIST_PU  (WR31 | (0 << 7))

// REG 34
#define WR34 ((0<<23) | (34 << 16) | (1 << 15) | (1 << 14) | (1 << 9) | (1 << 8)  | (1 << 7) | (1 << 6)  | (1 << 3) | (1 << 1))

// Enable entire channel divider
// Default: 1: enable
#define R34_CHDIV_ENABLE   (WR34 | (1 << 5))
#define R34_CHDIV_DISABLE  (WR34 | (0 << 5))

// REG 35
#define WR35 ((0<<23) | (35 << 16) | (1 << 4) | (1 << 3) | (1 << 0))

// Channel divider segment 2
// Default: 1
#define R35_CHDIV_SEG2_PD   (WR35 | (0 << 9))
#define R35_CHDIV_SEG2_D2   (WR35 | (1 << 9))
#define R35_CHDIV_SEG2_D4   (WR35 | (2 << 9))
#define R35_CHDIV_SEG2_D6   (WR35 | (4 << 9))
#define R35_CHDIV_SEG2_D8   (WR35 | (8 << 9))

// Channel divider segment 3
// 1: enable
// 0: power down (power down if not needed)
// Default: 0
#define R35_CHDIV_SEG3_ENABLE   (WR35 | (1 << 8))
#define R35_CHDIV_SEG3_DISABLE  (WR35 | (0 << 8))

// Channel divider segment 2
// 1: enable
// 0: power down (power down if not needed)
// Default: 0
#define R35_CHDIV_SEG2_ENABLE   (WR35 | (1 << 7))
#define R35_CHDIV_SEG2_DISABLE  (WR35 | (0 << 7))

// Channel divider segment 1
// Default: 1
#define R35_CHDIV_SEG1_D3   (WR35 | (1 << 2))
#define R35_CHDIV_SEG1_D2   (WR35 | (0 << 0))

// Channel divider segment 1
// 1: enable
// 0: power down (power down if not needed)
// Default: 0
#define R35_CHDIV_SEG1_ENABLE   (WR35 | (1 << 1))
#define R35_CHDIV_SEG1_DISABLE  (WR35 | (0 << 1))

// REG 36
#define WR36 ((0<<23) | (36 << 16))

// Enable buffer between channel divider and output B
// Default: 0 (disable)
#define R36_CHDIV_DISTB_ENABLE   (WR36 | (1 << 11))
#define R36_CHDIV_DISTB_DISABLE  (WR36 | (0 << 11))

// Enable buffer between channel divider and output A
// Default: 0 (disable)
#define R36_CHDIV_DISTA_ENABLE   (WR36 | (1 << 10))
#define R36_CHDIV_DISTA_DISABLE  (WR36 | (0 << 10))

// Channel divider segment select
// 4: includes channel divider segment 1,2 and 3
// 2: includes channel divider segment 1 and 2
// 1: includes channel divider segment 1
// 0: PD
// Default: 1
#define R36_CHDIV_SEG_SEL_PD   (WR36 | (0 << 4))
#define R36_CHDIV_SEG_SEL_D1   (WR36 | (1 << 4))
#define R36_CHDIV_SEG_SEL_D2   (WR36 | (2 << 4))
#define R36_CHDIV_SEG_SEL_D4   (WR36 | (4 << 4))

// Channel divider segment 3
// Default: 1
#define R36_CHDIV_SEG3_PD   (WR36 | (0 << 0))
#define R36_CHDIV_SEG3_D2   (WR36 | (1 << 0))
#define R36_CHDIV_SEG3_D4   (WR36 | (2 << 0))
#define R36_CHDIV_SEG3_D6   (WR36 | (4 << 0))
#define R36_CHDIV_SEG3_D8   (WR36 | (8 << 0))

// REG 37
#define WR37 ((0<<23) | (37 << 16) | (1 << 14))

// N-divider pre-scalar
// 1: divide-by-4
// 0: divide-by-2
// Default: 0
#define R37_PLL_N_PRE_D4   (WR37 | (1 << 12))
#define R37_PLL_N_PRE_D2   (WR37 | (0 << 12))

// REG 38
#define WR38 ((0<<23) | (38 << 16))

// Integer part of N-divider
// Default: 27
#define R38_PLL_N(x)        (WR38 | ((x & 0xFFF) << 1))

// REG 39
#define WR39 ((0<<23) | (39 << 16) | (1 << 15) | (1 << 2))

// PFD Delay
// 32: Not used
// 16: 16 clock cycle delay
// 8: 12 clock cycle delay
// 4: 8 clock cycle delay
// 2: 6 clock cycle delay
// 1: 4 clock cycle delay
// Default: 2 (6 clock cycle delay)
#define R39_PFD_DLY_4        (WR39 | (1 << 8))
#define R39_PFD_DLY_6        (WR39 | (2 << 8))
#define R39_PFD_DLY_8        (WR39 | (4 << 8))
#define R39_PFD_DLY_12       (WR39 | (8 << 8))
#define R39_PFD_DLY_16       (WR39 | (16 << 8))
#define R39_PFD_DLY_NU       (WR39 | (32 << 8))

// REG 40
#define WR40 ((0<<23) | (40 << 16))

// Denominator MSB of N-divider fraction
// Default: 1000
#define R40_PLL_DEN_MSB(x)        (WR40 | ((x & 0xFFFF) << 0))

// REG 41
#define WR41 ((0<<23) | (41 << 16))

// Denominator LSB of N-divider fraction
// Default: 1000
#define R41_PLL_DEN_LSB(x)        (WR41 | ((x & 0xFFFF) << 0))

// REG 42
#define WR42 ((0<<23) | (42 << 16))

// MASH seed MSB
// Default: 0
#define R42_MASH_SEED_MSB(x)        (WR42 | ((x & 0xFFFF) << 0))

// REG 43
#define WR43 ((0<<23) | (43 << 16))

// MASH seed LSB
// Default: 0
#define R43_MASH_SEED_LSB(x)        (WR43 | ((x & 0xFFFF) << 0))

// REG 44
#define WR44 ((0<<23) | (44 << 16))

// Numerator MSB of N-divider fraction
// Default: 0
#define R44_PLL_NUM_MSB(x)        (WR44 | ((x & 0xFFFF) << 0))

// REG 45
#define WR45 ((0<<23) | (45 << 16))

// Numerator LSB of N-divider fraction
// Default: 0
#define R45_PLL_NUM_LSB(x)        (WR45 | ((x & 0xFFFF) << 0))

// REG 46
#define WR46 ((0<<23) | (46 << 16) | (1 << 5))

// Output buffer A power
// increase power from 0 to 31
// extra boost from 48 to 63
// Default: 15
#define R46_OUTA_POW(x)        (WR46 | ((x & 0x3F) << 8))

// Output buffer B power down
// 1: power down
// 0: power up
#define R46_OUTB_PD  (WR46 | (1 << 7))
#define R46_OUTB_PU  (WR46 | (0 << 7))

// Output buffer A power down
// 1: power down
// 0: power up
#define R46_OUTA_PD  (WR46 | (1 << 6))
#define R46_OUTA_PU  (WR46 | (0 << 6))

// Sigma-delta modulator order
// 4: fourth order
// 3: third order
// 2: second order
// 1: first order
// 0: integer mode
// Default: 3 (third order)
#define R46_MASH_ORDER_INT      (WR46 | (0 << 0))
#define R46_MASH_ORDER_FIRTS    (WR46 | (1 << 0))
#define R46_MASH_ORDER_SECOND   (WR46 | (2 << 0))
#define R46_MASH_ORDER_THIDR    (WR46 | (3 << 0))
#define R46_MASH_ORDER_FOURTH   (WR46 | (4 << 0))

// REG 47
#define WR47 ((0<<23) | (47 << 16) | (1 << 7) | (1 << 6))

// Selects signal to the output buffer
// 2,3: reserved
// 1: Selects output from VCO
// 0: Selects the channel divider output
// Default: 0
#define R47_OUTA_MUX_CH   (WR47 | (0 << 11))
#define R47_OUTA_MUX_VCO  (WR47 | (1 << 11))

// Output buffer B power
// increase power from 0 to 31
// extra boost from 48 to 63
// Default: 0
#define R47_OUTB_POW(x)        (WR47 | ((x & 0x3F) << 0))

// REG 48
#define WR48 ((0<<23) | (48 << 16) | (0xFF << 2))

// Selects signal to the output buffer
// 2,3: reserved
// 1: Selects output from VCO
// 0: Selects the channel divider output
// Default: 0
#define R48_OUTB_MUX_CH   (WR48 | (0 << 0))
#define R48_OUTB_MUX_VCO  (WR48 | (1 << 0))

// REG 59
#define WR59 ((0<<23) | (59 << 16))

// This bit enables higher current output at MUXOUT pin if value is 1.
// Default: 0 (disable)
#define R59_MUXOUT_HDRV_ENABLE   (WR59 | (1 << 5))
#define R59_MUXOUT_HDRV_DISABLE  (WR59 | (0 << 5))

// REG 61
#define WR61 ((0<<23) | (61 << 16))

// To use lock detect, set MUXOUT_SEL=1. Use this register to
// select type of lock detect:
// 0: Calibration status detect (this indicates if the auto-calibration
// process has completed successfully and will output from
// MUXout pin a logic HIGH when successful). 1: vtune detect (this
// checks if vtune is in the expected range of voltages and outputs
// from MUXout pin a logic HIGH if device is locked and LOW if
// unlocked).
// Default: 1
#define R61_LD_TYPE_CALIB   (WR61 | (1 << 0))
#define R61_LD_TYPE_VTUNE   (WR61 | (0 << 0))

// REG 64
#define WR64 ((0<<23) | (64 << 16) | (1 << 4))

// Enable fast amplitude calibration
// Default: 0 (disable)
#define R64_ACAL_FAST_ENABLE   (WR64 | (1 << 9))
#define R64_ACAL_FAST_DISABLE  (WR64 | (0 << 9))

// Enable fast frequency calibration
// Default: 0 (disable)
#define R64_FCAL_FAST_ENABLE    (WR64 | (1 << 8))
#define R64_FCAL_FAST_DISABLE   (WR64 | (0 << 8))

// When ACAL_FAST=1, use this register to select the jump increment
// Default: 3, [7:5]
#define R64_AJUMP_SIZE(x)       (WR64 | ((x & 0x7) << 5))

// When FCAL_FAST=1, use this register to select the jump increment
// Default: 3, [3:0]
#define R64_FJUMP_SIZE(x)       (WR64 | ((x & 0x7) << 0))
