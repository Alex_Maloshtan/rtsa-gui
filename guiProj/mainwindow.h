#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <interfacelogic.h>
#include <QMainWindow>
#include <QTextEdit>
#include <QLineEdit>
#include <QLabel>
//#define COLUMNS 6
#include <QListWidget>
#include <QPushButton>
#include <QTableWidget>
#include <qlayout.h>
#include <qgroupbox.h>
#include <table.h>
#include <QShortcut>
#include <QJSEngine>
#include <iostream>
#include <jscore.h>
#include <QSplitter>

#include <QMenu>
using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class interfaceLogic;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    interfaceLogic logic;
    QSplitter split;
    Ui::MainWindow *ui;
    QVector<ntable*> ntables;
    QListWidget * list;
    QPushButton * addButton,*delButton,*moveUp,*moveDown,*execButton;
    QTableWidget * table;
    int curRow = 0;
    QStyle * windowsStyle;
    QMenu * menu;
    QGridLayout grid;
    QTextEdit * console;
    void clearLayout(QLayout *layout);
    ntable fill;
    void closeEvent(QCloseEvent * e);

private slots:
    void writeSettings();
    void readSettings();
    void onEdit(int row, int column);
    void onAddButton();
    void onDelButton();
    void onListClick(int row, int column);
    void onListMove(int row,int column,int pr,int pc);
    void onMoveUp();
    void onMoveDown();
    void onExec();
    void onClearTable();
    void onClearAll();
};

#endif // MAINWINDOW_H
