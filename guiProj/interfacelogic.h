#ifndef INTERFACELOGIC_H
#define INTERFACELOGIC_H
#include <QJSValue>
#include <QDebug>
#include <QGridLayout>
#include <QPushButton>
#include "table.h"
#include <QSettings>
#include <QCoreApplication>
#include <QJSEngine>
#include <QJSValue>
#include <defines.h>
#include <QFileDialog>
#include <binaryfilegenerator.h>
#include <QMainWindow>
class interfaceLogic
{
public:
    interfaceLogic();
    void execScript(QString text, const int chip, const bool reset, QString modeName);
    void addScenario(QVector<ntable *> & ntables, QTableWidget * scenarioList);
    void delScenario(QVector<ntable *> & ntables, QTableWidget * scenarioList,QGridLayout * gridLayout);
    int moveUp(QVector<ntable *> & ntables, QTableWidget * scenarioList);
    int moveDown(QVector<ntable *> & ntables, QTableWidget * scenarioList);

    int listClicked(int row, QVector<ntable *> & ntables, QGridLayout * gridLayout);
    void setupScenarioList(QTableWidget * table);
    void readFromFile(QString fileName);
    void execAllScripts(QVector<ntable *> ntables, QTableWidget *scenarioTable, QMainWindow *window);
    void onScenarioCellChanged(int row,int column,QString value);
    void setConsole(QTextEdit * console_);
    void setLayoutPtr(QGridLayout * ptr);
    QByteArray generateModeSettings();
    QVector<int> ads1Bytes;
    QVector<int> ads2Bytes;
    QVector<int> muxSettings;
    uint32_t lastAddressInFile;
public slots:
    void writeSettings(QVector<ntable *> & ntables, QTableWidget * table, QMainWindow  * window);
    void readSettings(QVector<ntable *> & ntables, QTableWidget * table, QMainWindow * window);
private:
    const QString fileNames[8] = {"rxfe_adfr6520.h","rxfe_ads5562.h","rxfe_digital.h","rxfe_hmc625.h",
                                 "rxfe_ltc2624.h","rxfe_ltc5586.h","rxfe_lxm2592.h","rxfe_pe43704.h"};
    const QString chipNames[9] = {"ADFR6520","ADS5562","HMC625","LXM2592_1","LXM2592_2","LTC2624","LTC5586","PE43704","DIGITAL"};
    QGridLayout * layoutPtr;
    QFile headerFile;
    QJSEngine eng;
    QJSValue coreVal;
    QSettings * settings;

    bool initDigitalInputWord = true;
    QVector<QVector<uint32_t>> ctrlWords;//для одного режима
    //QVector<QVector<QVector<uint32_t>>> ctrlWordsForMode;
    QVector<int> jumpTo;
    QVector<int> delays;
    QVector<int> triggers;
    QTextEdit * guiConsole;
    QVector<int> parseMux(QTableWidget *scenarioTable);
    int strToSizeInBytes(QString sizeStr);
    void moveFile(QString path, QString to);
    void moveFileForQSettings(QString path, QString to);
    void addDelay(QString text);
    void addTrigger(QString text);
};

#endif // INTERFACELOGIC_H
