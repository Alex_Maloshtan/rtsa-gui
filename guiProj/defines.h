#ifndef DEFINES_H
#define DEFINES_H

#define ADFR6520 0
#define ADS5562 1
#define HMC625 2
#define LXM2592_1 3
#define LXM2592_2 4
#define LTC2624 5
#define LTC5586 6
#define PE43704 7
#define DIGITAL 8

#define NOMUX 0
#define MUX1 1
#define MUX2 2
#define MUX1_2 3

#define KB_1000 1000
#define KB_1024 1024

#define NO_TRIGGER 0
#define TRIGGER_EN 1

// -------------------------



/*                   #define ADDR_ADFR6520 0x44A00000
                     #define ADDR_ADS5562 0x44A10000
                     #define ADDR_HMC625 0x44A20000
                     #define ADDR_LXM2592_1 0x44A30000
                     #define ADDR_LXM2592_2 0x44A40000
                     #define ADDR_LTC2624 0x44A50000
                     #define ADDR_LTC5586 0x44A60000
                     #define ADDR_PE43704 0x44A70000
                     #define ADDR_DIGITAL 0x44A80000
                     #define ADDR_SCENARIOS 0x0000*/

#endif // DEFINES_H
